import { Controller, Get, Param, UseGuards, UseFilters, Post, Delete, Put, ValidationPipe, Body, UnauthorizedException, HttpCode, HttpStatus } from '@nestjs/common';
import { UsersService } from '../core/services/users.service';
import { AuthGuard } from '@nestjs/passport';
import { ShowUserDTO } from '../models/user/user-show-dto';
import { UserIsDeletedFilter } from '../core/filters/userIsDeleted.filter';
import { UserNotFoundFilter } from '../core/filters/userNotFound.filter';
import { Roles } from '../common/decorators/roles.decorator';
import { RolesGuard } from '../common/guards/roles.guard';
import { UpdateUserDTO } from '../models/user/update-user-dto';
import { BlacklistGuard } from '../common/guards/blacklist.guard';
import { AuthUser } from '../decorators/user.decorator';
import { User } from '../data/entities/user';
import { ShowActivityDTO } from '../models/user/user-activity-dto';
import { UserRegisterDTO } from '../models/user/user-register-dto';
import { UserCreatedDTO } from '../models/user/user-create-dto';
import { DuplicateExceptionFilter } from '../core/filters/duplicateEntryFilter';
import { UpdateRelationDTO } from '../models/user/update-relation-dto';
import { CreateMessageDTO } from '../models/message/create-message-dto';
import { ShowMessageDTO } from '../models/message/show-message-dto';

@Controller('customers')
export class ActivityController {

  constructor(private readonly activityService: ActivityController) { }

}
