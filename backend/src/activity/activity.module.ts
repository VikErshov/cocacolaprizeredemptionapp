import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { Activity } from '../data/entities/activity';
import { Blacklist } from '../data/entities/blacklist';
import { Message } from '../data/entities/message';
import { ActivityController } from './activity.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([Activity]),
        CoreModule,
        AuthModule],
      controllers: [ActivityController],
})
export class UsersModule {}
