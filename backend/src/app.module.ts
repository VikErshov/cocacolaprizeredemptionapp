import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from './config/config.service';
import { UsersModule } from './users/users.module';
import { CustomersModule } from './customers/customers.module';
import { OutletModule } from './outlets/outlets.module';
import { ScannerModule } from './scanner/scanner.module';

// type, host, port, username, password, database, entities

@Module({
  imports: [
    UsersModule,
    CustomersModule,
    CoreModule,
    AuthModule,
    ConfigModule,
    OutletModule,
    ScannerModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: configService.dbType as any,
        host: configService.dbHost,
        port: configService.dbPort,
        username: configService.dbUsername,
        password: configService.dbPassword,
        database: configService.dbName,
        entities: ['./src/data/entities/**/*.ts'],
      }),
    }),
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
