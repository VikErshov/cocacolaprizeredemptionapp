import { Controller, Post, Body, ValidationPipe, BadRequestException, UseGuards, Delete } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersService } from '../core/services/users.service';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { UserRegisterDTO } from '../models/user/user-register-dto';
import { User } from '../data/entities/user';
import { plainToClass } from 'class-transformer';
import { ShowUserDTO } from '../models/user/user-show-dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { Roles } from '../decorators/roles.decorator';
import { BlacklistGuard } from '../common/guards/blacklist.guard';
import { AuthUser } from '../decorators/user.decorator';
import { Token } from '../common/decorators/token.decorator';

@Controller('')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) { }

  @Post('login')
  async login(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) user: UserLoginDTO): Promise<{ token: string, fullUser: ShowUserDTO }> {
    const token = await this.authService.signIn(user);
    if (!token) {
      throw new BadRequestException(`Wrong credentials!`);
    }
    let fullUser = await this.usersService.findUserByName(user.name);
    fullUser = plainToClass(ShowUserDTO, { ...fullUser }, { excludeExtraneousValues: true })

    return { token, fullUser };
  }

  @Post('register')
  // @UseGuards(AuthGuard(), RolesGuard)
  // @Roles('admin')
  async register(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) user: UserRegisterDTO): Promise<User> {
    return await this.usersService.register(user);
  }

  @Delete('logout')
  @UseGuards(AuthGuard(), BlacklistGuard)
  public async logOut(@AuthUser() user: any, @Token() token: string) {
    return await this.authService.logOut(user, token);
  }
}
