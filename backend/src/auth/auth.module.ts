import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { CoreModule } from '../core/core.module';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy/jwt.strategy';
import { ConfigModule } from '../config/config.module';
import { config } from '../common/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user';
import { Activity } from '../data/entities/activity';
import { Blacklist } from '../data/entities/blacklist';
import { ConfigService } from '../config/config.service';

@Module({
  imports: [
    CoreModule,
    ConfigModule,
    PassportModule.register({defaultStrategy: 'jwt'}),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      // Use the actual ConfigService, not the hardcoded config object
      useFactory: async (configService: ConfigService) => ({
        secretOrPrivateKey: configService.jwtSecret,
        signOptions: {
          expiresIn: configService.jwtExpireTime, // one hour
        },
      }),
    }),TypeOrmModule.forFeature([User, Activity, Blacklist])
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  exports: [AuthService, PassportModule],
})
export class AuthModule {}
