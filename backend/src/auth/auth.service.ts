import { Injectable, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../core/services/users.service';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { JwtPayload } from '../core/interfaces/jwt-payload';
import { User } from '../data/entities/user';
import { InjectRepository } from '@nestjs/typeorm';
import { Blacklist } from '../data/entities/blacklist';
import { Repository } from 'typeorm';
import { decode } from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import { UserNotFoundError } from '../core/customErrors/user-not-found.error';
import { InvalidPasswordException } from '../core/customErrors/invalidPassword';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
    @InjectRepository(Blacklist) private readonly blacklistRespository: Repository<Blacklist>,
    @InjectRepository(User) private readonly userRepo: Repository<User>
  ) {}

  public async findUserByUsername(name: string): Promise<User> | undefined {
    const user = await this.userRepo.findOne({ name });
    return user;
  }

   public async validateUserPass(user, userEntity): Promise<boolean> {
    return await bcrypt.compare(user.password, userEntity.password);
  }

  async validateUser(payload: JwtPayload): Promise<User | undefined> {
    return await this.usersService.validate(payload);
  }


  public async signIn(user: UserLoginDTO): Promise<any> {

    
		const checkExist = await this.findUserByUsername(user.name);

		if (!checkExist) {
			throw new UserNotFoundError('User with such username doesnt exist!');
		}

		if (!(await this.validateUserPass(user, checkExist))) {
			throw new InvalidPasswordException('Invalid password!', HttpStatus.FORBIDDEN);
		}


		const payload = { name: user.name };
		const token = await this.jwtService.signAsync(payload);

		return await this.jwtService.sign({ name: checkExist.name });
	}

  

  public async logOut(user: User, token: string) {
		
		const decodedToken: any = decode(token);
		const expiresOn = decodedToken.exp;
		const newBlacklistedToken = {
			token,
			expiresOn
		};
	
		await this.blacklistToken(user, newBlacklistedToken)
	
		return await 'logged out';
  }
  
  public async blacklistToken(
		user: User,
		tokenObj: {token: string, expiresOn: string}
	) {
		const newToken = this.blacklistRespository.create(tokenObj);
		newToken.user = Promise.resolve(user);

		return this.blacklistRespository.save(newToken);
	}
}
