import { createParamDecorator } from "@nestjs/common";

export const Token = createParamDecorator((data, req: any) => {
     return req.headers.authorization.replace('Bearer', '').trim();
});