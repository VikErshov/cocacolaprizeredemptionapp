import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class BlacklistGuard implements CanActivate {
     constructor(private reflector: Reflector) { }

     async canActivate(context: ExecutionContext): Promise<boolean> {
          const req = context.switchToHttp().getRequest();
          const user = req.user;
          const token = req.headers.authorization.replace('Bearer', '').trim();
          const blacklistTokens = await user.blacklist;

          if (blacklistTokens.find(blacklistedToken => blacklistedToken.token === token)) {
               return false;
          }

          return true;
     }
}
