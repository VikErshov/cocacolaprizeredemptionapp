import { Module } from '@nestjs/common';
import { UsersService } from './services/users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user';
import { CustomersService } from './services/customers.service';
import { Customer } from '../data/entities/customer';
import { Outlet } from '../data/entities/outlet';
import { OutletService } from './services/outlets.service';
import { Activity } from '../data/entities/activity';
import { Blacklist } from '../data/entities/blacklist';
import { ScannerService } from './services/scanner.service';
import { Barcode } from '../data/entities/barcode';
import { Prize } from '../data/entities/prize';
import { RedemptionRecord } from '../data/entities/prizeRedemption';
import { Message } from '../data/entities/message';

@Module({
  imports: [TypeOrmModule.forFeature([User, Customer, Outlet, Activity, Barcode, Prize, RedemptionRecord, Message])],
  providers: [UsersService, CustomersService, OutletService, ScannerService],
  exports: [UsersService, CustomersService, OutletService, ScannerService],
})
export class CoreModule { }
