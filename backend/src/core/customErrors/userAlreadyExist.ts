import { BaseException } from "./user-base-exception";

export class UserAlreadyExistException extends BaseException{}