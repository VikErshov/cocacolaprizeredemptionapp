import { Catch, HttpException, ArgumentsHost, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { BarcodeAlreadyRedeemed } from '../customErrors/barcode-alreadyRedeemed';

@Catch(BarcodeAlreadyRedeemed)

export class BarcodeAlreadyRedeemedFilter implements ExceptionFilter {

     public catch(exception: HttpException, host: ArgumentsHost) {
          const ctx = host.switchToHttp();
          const response = ctx.getResponse();
          const request = ctx.getRequest();

          response.status(HttpStatus.NOT_FOUND).json({
               error: exception.message,
          });
     }
}
