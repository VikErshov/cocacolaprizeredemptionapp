import { Catch, HttpException, ArgumentsHost, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { CustomerNotFound } from '../customErrors/customer-notFound';

@Catch(CustomerNotFound)

export class CustomerNotFoundFilter implements ExceptionFilter {

     public catch(exception: HttpException, host: ArgumentsHost) {
          const ctx = host.switchToHttp();
          const response = ctx.getResponse();
          const request = ctx.getRequest();

          response.status(HttpStatus.NOT_FOUND).json({
               statusCode: HttpStatus.NOT_FOUND,
               error: exception.message,
          });
     }
}
