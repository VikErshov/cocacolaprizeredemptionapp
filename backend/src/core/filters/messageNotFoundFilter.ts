import { Catch, HttpException, ArgumentsHost, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { BarcodeNotFound } from '../customErrors/barcode-notFound';
import { MessageNotFound } from '../customErrors/message-not-found';

@Catch(MessageNotFound)

export class MessageNotFoundFilter implements ExceptionFilter {

     public catch(exception: HttpException, host: ArgumentsHost) {
          const ctx = host.switchToHttp();
          const response = ctx.getResponse();
          const request = ctx.getRequest();

          response.status(HttpStatus.NOT_FOUND).json({
               error: exception.message,
          });
     }
}
