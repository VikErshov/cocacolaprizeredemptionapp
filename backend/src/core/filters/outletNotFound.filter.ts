import { Catch, HttpException, ArgumentsHost, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { OutletNotFound } from '../customErrors/outlet-not-found';

@Catch(OutletNotFound)

export class OutletsNotFound implements ExceptionFilter {

     public catch(exception: HttpException, host: ArgumentsHost) {
          const ctx = host.switchToHttp();
          const response = ctx.getResponse();
          const request = ctx.getRequest();

          response.status(HttpStatus.NOT_FOUND).json({
               statusCode: HttpStatus.NOT_FOUND,
               error: exception.message,
          });
     }
}
