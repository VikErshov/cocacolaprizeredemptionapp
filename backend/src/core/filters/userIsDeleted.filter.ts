import { Catch, HttpException, ArgumentsHost, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { UserIsDeletedError } from '../customErrors/user-is-deleted.error';

@Catch(UserIsDeletedError)

export class UserIsDeletedFilter implements ExceptionFilter {

  public catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    response.status(HttpStatus.NOT_FOUND).json({
      statusCode: HttpStatus.NOT_FOUND,
      error: exception.message,
    });
  }
}
