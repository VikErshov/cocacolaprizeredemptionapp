import { Catch, HttpException, ArgumentsHost, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { UserNotFoundError } from '../customErrors/user-not-found.error';

@Catch(UserNotFoundError)

export class UserNotFoundFilter implements ExceptionFilter {

  public catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    response.status(HttpStatus.NOT_FOUND).json({
      statusCode: HttpStatus.NOT_FOUND,
      error: exception.message,
    });
  }
}
