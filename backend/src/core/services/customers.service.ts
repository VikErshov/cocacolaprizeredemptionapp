import { Injectable, Post, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserRole } from '../../common/enums/user-role.enum';
import { ShowUserDTO } from '../../models/user/user-show-dto';
import { plainToClass } from 'class-transformer';
import { Customer } from '../../data/entities/customer';
import { CreateCustomerDTO } from '../../models/customer/create-customer-dto';
import { DuplicateEntry } from '../customErrors/duplicate-outlets';
import { ShowCustomerDTO } from '../../models/customer/show-customer-dto';
import { CustomerNotFoundFilter } from '../filters/customerNotFound';
import { CustomerNotFound } from '../customErrors/customer-notFound';
import { AlreadyDeleted } from '../customErrors/outlet-is-deleted';
import { UpdateCustomerDTO } from '../../models/customer/update-customer';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer) private readonly customerRepository: Repository<Customer>,
  ) { }

  async createCustomer(customer: CreateCustomerDTO): Promise<ShowCustomerDTO> {
    const newCustomer = new Customer();
    newCustomer.name = customer.name;

    const duplicateCustomerName = await this.customerRepository.findOne({
      where: {
        name: customer.name,
      },
    });

    if (!!duplicateCustomerName) {
      throw new DuplicateEntry(`Customer with this name ${customer.name} already exists`);
    }

    const savedCustomer = await this.customerRepository.save(newCustomer);
    const createdCustomer: ShowCustomerDTO = plainToClass(ShowCustomerDTO, { ...customer }, { excludeExtraneousValues: true })
    return await savedCustomer;
  }

  async deleteOutlet(id: string): Promise<ShowCustomerDTO> {
    const customerFounded = await this.customerRepository.findOne({ where: { id } });

    const deletedCustomer = { ...customerFounded, isDeleted: true };
    console.log(customerFounded);
    if (!customerFounded) {
      throw new CustomerNotFound('Customer is not found');
    }

    if (customerFounded.isDeleted === true) {
      throw new AlreadyDeleted('Customer is already deleted!');
    }

    return plainToClass(ShowCustomerDTO, await this.customerRepository.save({ ...deletedCustomer }), { excludeExtraneousValues: true });
  }

  async getCustomer(id: string): Promise<ShowCustomerDTO> {
    const founded = await this.customerRepository.findOne({ where: { id } });

    if (!founded) {
      throw new CustomerNotFound('Customer is not found');
    }

    const customerToShow: ShowCustomerDTO = plainToClass(ShowCustomerDTO, { ...founded }, { excludeExtraneousValues: true })
    return customerToShow;
  }

  async getAll(): Promise<ShowCustomerDTO[]> {
    const allCustomers = await this.customerRepository.find(
      {
        where: { isDeleted: false },
      },
    );

    console.log(allCustomers);
    if (!allCustomers) {
      throw new CustomerNotFound('Customer not found!');
    }

    return allCustomers.map((customers: any) => plainToClass(ShowCustomerDTO, { ...customers }, { excludeExtraneousValues: true }));
  }

  async updateCustomer(customerId: string, name: UpdateCustomerDTO): Promise<ShowCustomerDTO> {
    const foundedCustomer = await this.customerRepository.findOne({ where: { id: customerId } });
    console.log(foundedCustomer);


    if (!foundedCustomer) {
      throw new CustomerNotFound('Customer not found!')
    }

    const updateCustomer = await this.customerRepository.save({ ...foundedCustomer, ...name });
    console.log(updateCustomer);
    const customerToShow: ShowCustomerDTO = plainToClass(ShowCustomerDTO, { ...updateCustomer }, { excludeExtraneousValues: true });
    return customerToShow;
  }
}