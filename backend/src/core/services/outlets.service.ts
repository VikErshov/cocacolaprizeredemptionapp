import { InjectRepository } from "@nestjs/typeorm";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { Outlet } from "../../data/entities/outlet";
import { Repository } from "typeorm";
import { User } from "../../data/entities/user";
import { ShowOutletDTO } from "../../models/outlet/outlet-show-dto";
import { plainToClass } from "class-transformer";
import { OutletNotFound } from "../customErrors/outlet-not-found";
import { OutletUpdateDTO } from "../../models/outlet/outlet-update-dto";
import { CreateOutletDTO } from "../../models/outlet/outlet-create-dto";
import { BaseExceptionFilter } from "@nestjs/core";
import { DuplicateEntry } from "../customErrors/duplicate-outlets";
import { AlreadyDeleted } from "../customErrors/outlet-is-deleted";
import { Customer } from "../../data/entities/customer";

@Injectable()
export class OutletService {
     constructor(
          @InjectRepository(Outlet) private readonly outletRepo: Repository<Outlet>,
          @InjectRepository(Customer) private readonly customerRepo: Repository<Customer>
     ) { }
     async createOutlet(outlet: CreateOutletDTO): Promise<ShowOutletDTO> {
          const newOutlet = new Outlet();
          const customer = await this.customerRepo.findOne({ name: outlet.title });
          console.log(customer);

          newOutlet.title = outlet.title;
          newOutlet.location = outlet.location;
          newOutlet.brand = Promise.resolve(customer);

          const duplicateLocationOutlet = await this.outletRepo.findOne({
               where: {
                    location: outlet.location,
               },
          });

          if (!!duplicateLocationOutlet) {
               throw new DuplicateEntry(`Outlet with this location ${outlet.location} already exists`);
          }

          const savedOutlet = await this.outletRepo.save(newOutlet);
          const createdOutlet: ShowOutletDTO = plainToClass(ShowOutletDTO, { ...outlet }, { excludeExtraneousValues: true })
          return await createdOutlet;
     }

     async deleteOutlet(outletId: string, customerId: string): Promise<ShowOutletDTO> {

          const outletFounded = await this.outletRepo.findOne({ where: { id: outletId, brand: customerId } });
          console.log(outletId);
          console.log(customerId);
          const deletedOutlet = { ...outletFounded, isDeleted: true };
          console.log(outletFounded);
          if (!outletFounded) {
               throw new OutletNotFound('Outlet is not found');
          }

          if (outletFounded.isDeleted === true) {
               throw new AlreadyDeleted('Outlet is already deleted!');
          }

          return plainToClass(ShowOutletDTO, await this.outletRepo.save({ ...deletedOutlet }), { excludeExtraneousValues: true });
     }

     async getOutlet(customerId: string, outletsId: string): Promise<ShowOutletDTO> {
          const founded = await this.outletRepo.findOne({ where: { id: outletsId, brand: customerId } });
          console.log(founded);
          

          if (!founded) {
               throw new OutletNotFound('Outlet is not found');
          }

          const outletToShow: ShowOutletDTO = plainToClass(ShowOutletDTO, { ...founded }, { excludeExtraneousValues: true })
          return outletToShow;
     }
     //customers/:customersId/outlets/:outletsId
     async getAllfromOneBrand(customerId: string): Promise<ShowOutletDTO[]> {

          const outlet = new Outlet();
          const customer = await this.customerRepo.find({ where: { customerId } });
          console.log(customerId)

          const allOutlets = await this.outletRepo.find(
               {
                    where: { isDeleted: false, brand: customerId }
               },
          );

          if (!allOutlets) {
               throw new OutletNotFound('Outlets not found!');
          }

          return allOutlets.map((outlets: any) => plainToClass(ShowOutletDTO, { ...outlets }, { excludeExtraneousValues: true }));
     }

     async updateOutlet(customerId: string, outletId: string, location: OutletUpdateDTO): Promise<ShowOutletDTO> {
          const foundedOutlet = await this.outletRepo.findOne({where: { id: outletId, brand: customerId }});
          console.log(await foundedOutlet.user);
          
          
          if (!foundedOutlet) {
               throw new OutletNotFound('Outlet not found!')
          }

          const updateOutlet = await this.outletRepo.save({ ...foundedOutlet, ...location });
          const outletToShow: ShowOutletDTO = plainToClass(ShowOutletDTO, { ...updateOutlet }, { excludeExtraneousValues: true })
          return outletToShow;
     }

}