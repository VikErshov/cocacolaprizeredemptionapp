import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Barcode } from "../../data/entities/barcode";
import { Prize } from "../../data/entities/prize";
import { DuplicateEntry } from "../customErrors/duplicate-outlets";
import { plainToClass } from "class-transformer";
import { User } from "../../data/entities/user";
import { RedemptionRecord } from "../../data/entities/prizeRedemption";
import { ShowBarcodeDTO } from "../../models/barcode/show-barcode-dto";
import { Outlet } from "../../data/entities/outlet";
import { PrizeRedemptionDTO } from "../../models/redemptionRecord/prizeRedemption-dto";
import { ScanBarcodeDTO } from "../../models/barcode/scan-barcode-dto";
import { PrizeEnum } from "../../common/enums/prize.enum";
import { BarcodeAlreadyRedeemed } from "../customErrors/barcode-alreadyRedeemed";
import { Customer } from "../../data/entities/customer";

@Injectable()
export class ScannerService {
     constructor(
          @InjectRepository(Barcode) private readonly barcodeRepo: Repository<Barcode>,
          @InjectRepository(Prize) private readonly prizeRepository: Repository<Prize>,
          @InjectRepository(User) private readonly userRepository: Repository<User>,
          @InjectRepository(RedemptionRecord) private readonly redRecordRepo: Repository<RedemptionRecord>,
          @InjectRepository(Outlet) private readonly outletRepo: Repository<Outlet>,
          @InjectRepository(Customer) private readonly customerRepo: Repository<Customer>,
     ) { }

     async scan(barcode: string, userId: string): Promise<any> {

          const foundedBarcode = await this.barcodeRepo.findOne({ where: { barcode: barcode } })
          console.log(foundedBarcode);
          if (!foundedBarcode) {
               return `You win smile :)`;
          }

          return barcode;

     }

     async redeemPrize(userId: string, barcode: ScanBarcodeDTO,prize:string): Promise<PrizeRedemptionDTO> {
          const newPrizeRedemption = new RedemptionRecord();
          console.log(1);
          
          console.log(barcode);
          const foundedBarcode = await this.barcodeRepo.findOne({ where: { barcode: barcode.barcode } });
          const foundedPrize = await this.prizeRepository.findOne({ where: { name: prize  } });
          console.log(2);
          
          console.log(prize)
          const user = await this.userRepository.findOne({ where: { id: userId } });
          console.log(3);
          console.log(await foundedPrize);
          const outlet = await user.outlet;
          const foundedOutlet = await this.outletRepo.findOne({
               where: {id: outlet.id}});
          console.log(4);
          console.log(foundedOutlet);
          const customer =await outlet.brand;
          console.log(customer);
          const foundedCustomer = await this.customerRepo.findOne({where: {id: customer.id}});
          console.log(foundedCustomer);

          newPrizeRedemption.barcode = Promise.resolve(foundedBarcode);
          newPrizeRedemption.user = Promise.resolve(user);
          newPrizeRedemption.prize = Promise.resolve(foundedPrize);
          newPrizeRedemption.outlet = Promise.resolve(user.outlet);
          newPrizeRedemption.customer = Promise.resolve(foundedCustomer);
          console.log(222222)
          // console.log(createdRecord);
          
          if (foundedBarcode.isRedeem === false) {
               foundedBarcode.isRedeem = true;
               foundedPrize.quantity -= 1;
               const savedQuantity = await this.prizeRepository.save(foundedPrize)
               const savedBarcode = await this.barcodeRepo.save(foundedBarcode);
               const savedRedemptionRecord = await this.redRecordRepo.save(newPrizeRedemption);   
               const createdRecord: PrizeRedemptionDTO = plainToClass(PrizeRedemptionDTO, {...savedRedemptionRecord,outlet: outlet.location,barcode:barcode.barcode,user:user.name,prize:foundedPrize.name,customer:customer.name }, { excludeExtraneousValues: true })
               console.log(createdRecord);
               // return `Congratulations, you win a ${await foundedPrize.name}`
               const createdMessage: ShowBarcodeDTO = plainToClass(ShowBarcodeDTO, {...foundedPrize,foundedBarcode }, { excludeExtraneousValues: true })
               console.log(createdMessage);
               
               return createdRecord;
          }

          if (foundedBarcode.isRedeem === true) {
               throw new BarcodeAlreadyRedeemed(`This barcode: ${barcode.barcode} has already been redeemed `)
          }
          
     }

     async barcode(barcode:ScanBarcodeDTO): Promise<ScanBarcodeDTO>{
          return this.barcodeRepo.save(barcode);
     }

     async getPrizes(): Promise<any> {
          const founded = await this.prizeRepository.find();
          console.log(founded);
          
          return founded;
     }

     async isRedeem(barcode:string): Promise<any>{
          const foundedBarcode = await this.barcodeRepo.findOne({where: {barcode: barcode}});
          console.log(barcode);
          
          return foundedBarcode.isRedeem;
     }
}
