import { Injectable, Post, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { User } from '../../data/entities/user';
import { UserLoginDTO } from '../../models/user/user-login-dto';
import { JwtPayload } from '../interfaces/jwt-payload';
import { UserRegisterDTO } from '../../models/user/user-register-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserRole } from '../../common/enums/user-role.enum';
import { ShowUserDTO } from '../../models/user/user-show-dto';
import { plainToClass } from 'class-transformer';
import { UserIsDeletedError } from '../customErrors/user-is-deleted.error';
import { UserNotFoundError } from '../customErrors/user-not-found.error';
import { UpdateUserDTO } from '../../models/user/update-user-dto';
import { ShowActivityDTO } from '../../models/user/user-activity-dto';
import { Activity } from '../../data/entities/activity';
import { Outlet } from '../../data/entities/outlet';
import { UserCreatedDTO } from '../../models/user/user-create-dto';
import * as bcrypt from 'bcrypt';
import { DuplicateEntry } from '../customErrors/duplicate-outlets';
import { Customer } from '../../data/entities/customer';
import { OutletNotFound } from '../customErrors/outlet-not-found';
import { UpdateRelationDTO } from '../../models/user/update-relation-dto';
import { Message } from '../../data/entities/message';
import { CreateMessageDTO } from '../../models/message/create-message-dto';
import { ShowMessageDTO } from '../../models/message/show-message-dto';
import { MessageNotFound } from '../customErrors/message-not-found';
import { UserAlreadyExistException } from '../customErrors/userAlreadyExist';
import { RedemptionRecord } from '../../data/entities/prizeRedemption';
import { PrizeRedemptionDTO } from '../../models/redemptionRecord/prizeRedemption-dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Outlet) private readonly outletRepo: Repository<Outlet>,
    @InjectRepository(Customer) private readonly customerRepo: Repository<Customer>,
    @InjectRepository(RedemptionRecord) private readonly redemptionRecRepo: Repository<RedemptionRecord>,
    @InjectRepository(Activity) private readonly activityRepository: Repository<Activity>,
    @InjectRepository(Message) private readonly messageRepository: Repository<Message>,
  ) { }

  public async hashPassword(password): Promise<string> {
    return await bcrypt.hash(password, 10)
  }

  async createUser(user: UserCreatedDTO, outletId: string): Promise<ShowUserDTO> {
    const newUser = new User();

    const outlet = await this.outletRepo.findOne({ id: outletId });

    console.log(outlet);
    console.log(user);
    newUser.name = user.name;
    newUser.email = user.email;
    newUser.password = await bcrypt.hash(user.password, 10);
    newUser.outlet = Promise.resolve(outlet);

    const duplicateUserName = await this.userRepository.findOne({
      where: {
        name: user.name,
      },
    });

    const duplicateUserEmail = await this.userRepository.findOne({
      where: {
        email: user.email,
      },
    });

    if (!!duplicateUserName) {
      throw new DuplicateEntry(`User with this name ${user.name} already exists`);
    }

    if (!!duplicateUserEmail) {
      throw new DuplicateEntry(`User with this email ${user.email} already exists`);
    }

    const savedUser = await this.userRepository.save(newUser);
    const createdUser: ShowUserDTO = plainToClass(ShowUserDTO, { ...user }, { excludeExtraneousValues: true })
    return await createdUser;
  }

  async getAllFromOneOutlet(customerId: string, outletId: string): Promise<ShowUserDTO[]> {

    const outlet = await this.outletRepo.findOne({ where: { id: outletId, brand: outletId } });
    console.log(outlet);

    console.log(customerId);
    const allUsers = await this.userRepository.find(
      {
        where: { isDeleted: false, outlet: outletId },
      },
    );


    if (!allUsers) {
      throw new UserNotFoundError('Users not found!');
    }


    return allUsers.map((user: any) => plainToClass(ShowUserDTO, { ...user }, { excludeExtraneousValues: true }));
  }

  public async getUserById(customerId: string, outletId: string, id: string): Promise<ShowUserDTO> {
    const foundedOutlet = await this.outletRepo.findOne({ where: { brand: customerId } })
    const foundUser = await this.userRepository.findOne({
      where: {
        id: id,
        isDeleted: false,
        outlet: outletId
      },
    });
    console.log(foundedOutlet);

    console.log(foundUser);

    console.log(outletId);
    console.log(customerId)
    console.log(Promise.resolve(foundedOutlet.brand));
    if (!foundUser) {
      throw new UserNotFoundError('User not found!');
    }

    return await plainToClass(ShowUserDTO, { ...foundUser }, { excludeExtraneousValues: true });
  }

  public async updateSpecificUser(updatedUserDTO: UpdateUserDTO, userId: string): Promise<ShowUserDTO> {

    const userToUpdate = await this.findUserById(userId);
    console.log(userToUpdate);
    if (!userToUpdate) {
      throw new UserNotFoundError('User not found!');
    }

    const updatedUser = await this.userRepository.save({ ...userToUpdate, ...updatedUserDTO });
    const userToShow: ShowUserDTO = plainToClass(ShowUserDTO, { ...updatedUser }, { excludeExtraneousValues: true });
    return userToShow;
  }

  public async changeRelation(updatedRelation: UpdateRelationDTO, userId: string): Promise<ShowUserDTO> {

    const userToUpdate = await this.findUserById(userId);

    if (!userToUpdate) {
      throw new UserNotFoundError('User not found!');
    }

    const outlet = await this.findOutletByLocation(updatedRelation.newRelation);

    if (!userToUpdate) {
      throw new OutletNotFound('Outlet not found!');
    }

    const a = await userToUpdate.outlet;

    userToUpdate.outlet = Promise.resolve(outlet);

    const updatedUser = await this.userRepository.save(userToUpdate);
    const userToShow: ShowUserDTO = plainToClass(ShowUserDTO, { ...updatedUser }, { excludeExtraneousValues: true });
    return userToShow;
  }

  public async deleteUser(userId: string): Promise<ShowUserDTO> {
    const foundUser = await this.findUserById(userId);
    if (!foundUser) {
      throw new UserNotFoundError('User not found!');
    }
    if (foundUser.isDeleted === true) {
      throw new UserIsDeletedError('This user has been deleted!');
    }
    const updatedUser = { ...foundUser, isDeleted: true };
    return plainToClass(ShowUserDTO, await this.userRepository.save({ ...updatedUser }), { excludeExtraneousValues: true });
  }

  async signIn(user: UserLoginDTO): Promise<User | undefined> {
    return await this.userRepository.findOne({
      where: {
        ...user,
      },
    });
  }

  public async register(user: UserRegisterDTO): Promise<User> {

    const checkExist: User = await this.userRepository.findOne(user.name);

    if (!!checkExist) {
      throw new UserAlreadyExistException('User with such username already exists!')
    }

    const passwordHash = await this.hashPassword(user.password);
    user.password = passwordHash;

    const newUser = await this.userRepository.save(user)

    return newUser
  }

  async validate(payload: JwtPayload): Promise<User | undefined> {
    return await this.userRepository.findOne({
      where: {
        ...payload,
      },
    });
  }

  public async findUserByName(name: string): Promise<ShowUserDTO> {
    const foundUser = await this.userRepository.findOne({
      where: {
        name,
        isDeleted: false,
      },
    });

    if (!foundUser) {
      throw new UserNotFoundError('User not found!');
    }

    return await plainToClass(ShowUserDTO, { ...foundUser }, { excludeExtraneousValues: true });
  }

  public async findUserById(id: string): Promise<User> {
    return await this.userRepository.findOne({
      where: {
        id,
      },
    });
  }

  public async findOutletByLocation(location: string): Promise<Outlet> {
    return await this.outletRepo.findOne({
      where: {
        location,
      },
    });
  }

  public async createMessage(message: CreateMessageDTO): Promise<ShowMessageDTO> {
    const myUser: User = await this.userRepository.findOne({name: message.username});
    const messageToSave = new Message();

    if (!myUser) {
      throw new UserNotFoundError('No such user exists!');
    }

    messageToSave.issueTitle = message.issueTitle;
    messageToSave.description = message.description;
    messageToSave.user = Promise.resolve(myUser);

    const savedMessage = await this.messageRepository.save(messageToSave);
    const messageToReturn: ShowMessageDTO = plainToClass(ShowMessageDTO, { ...savedMessage }, { excludeExtraneousValues: true });
    return await messageToReturn;

  }

  public async getMessagesForAll(): Promise<ShowMessageDTO[]> {
    const messages = await this.messageRepository.find({
      where: {
        isDeleted: false,
      },
    });

    if (!messages) {
      throw new MessageNotFound('No messages found');
    }

    return messages.map((message: any) => plainToClass(ShowMessageDTO, { ...message }, { excludeExtraneousValues: true }));
  }

  public async readMessage(messageId: string): Promise<ShowMessageDTO> {
    const messageToRead = await this.messageRepository.findOne({id: messageId});

    const messageIsRead = { ...messageToRead, isRead: true };
    return plainToClass(ShowMessageDTO, await this.messageRepository.save({ ...messageIsRead }), { excludeExtraneousValues: true });
  }

  public async unreadMessage(messageId: string): Promise<ShowMessageDTO> {
    const messageToRead = await this.messageRepository.findOne({id: messageId});

    const messageIsRead = { ...messageToRead, isRead: false };
    return plainToClass(ShowMessageDTO, await this.messageRepository.save({ ...messageIsRead }), { excludeExtraneousValues: true });
  }

  public async deleteMessage(messageId: string): Promise<ShowMessageDTO> {
    const messageToDelete = await this.messageRepository.findOne({id: messageId});

    const messageIsDeleted = { ...messageToDelete, isDeleted: true };
    return plainToClass(ShowMessageDTO, await this.messageRepository.save({ ...messageIsDeleted }), { excludeExtraneousValues: true });
  }

  // public async getActivity(id: string, user: User): Promise<ShowActivityDTO[]> {
  //   const myUser: User = await this.userRepository.findOne({ id });

  //   if (user.roles === 'admin') {
  //     return await this.activityRepository.find({ where: { user: 'admin' } });
  //   }

  //   if (id !== user.id && user.roles !== 'admin') {
  //     throw new BadRequestException(`Only administrators can view user's activity`);
  //   }
  // }

  public async getRedemptionRecordByUser(userId:string): Promise<PrizeRedemptionDTO[]>{
    const foundedRedemptions = await this.redemptionRecRepo.find({
      relations: ['customer','outlet','user','prize','barcode'],
      where: {userId: userId}});
    console.log(foundedRedemptions);
    const showRedemptions = foundedRedemptions.map((redRec:any) => plainToClass(PrizeRedemptionDTO, {...redRec,customer: redRec.__customer__.name,outlet: redRec.__outlet__.location,user: redRec.__user__.name,prize: redRec.__prize__.name,barcode:redRec.__barcode__.barcode}))
    console.log(2323);
    console.log(showRedemptions);
    return await showRedemptions; 
  }
}
