import { Controller, Get, Param, UseGuards, UseFilters, Post, Delete, Put, ValidationPipe, Body, HttpCode, HttpStatus } from '@nestjs/common';
import { UsersService } from '../core/services/users.service';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../common/decorators/user.decorator';
import { ShowUserDTO } from '../models/user/user-show-dto';
import { Roles } from '../common/decorators/roles.decorator';
import { RolesGuard } from '../common/guards/roles.guard';
import { CustomersService } from '../core/services/customers.service';
import { CreateCustomerDTO } from '../models/customer/create-customer-dto';
import { DuplicateExceptionFilter } from '../core/filters/duplicateEntryFilter';
import { ShowCustomerDTO } from '../models/customer/show-customer-dto';
import { AlreadyDeletedFilter } from '../core/filters/outletIsAlreadyDeleted';
import { CustomerNotFoundFilter } from '../core/filters/customerNotFound';
import { UpdateCustomerDTO } from '../models/customer/update-customer';
import { BlacklistGuard } from '../common/guards/blacklist.guard';

@Controller('customers')
export class CustomersController {

  constructor(private readonly customersService: CustomersService) { }

  @Post('')
  @UseGuards(AuthGuard(), RolesGuard, BlacklistGuard)
  @UseFilters(DuplicateExceptionFilter)
  @Roles('admin')
  public async createCustomer(@Body(new ValidationPipe({ transform: true, whitelist: true })) customer: CreateCustomerDTO): Promise<ShowCustomerDTO> {
    return this.customersService.createCustomer(customer);
  }

  @Delete('/:customerId')
  @UseGuards(AuthGuard(),RolesGuard, BlacklistGuard)
  @Roles('admin')
  @UseFilters(AlreadyDeletedFilter)
  async deleteOutlet(@Param('customerId') customerId: string): Promise<ShowCustomerDTO> {
    return await this.customersService.deleteOutlet(customerId);
  }

  @Get('/:customerId')
  @UseGuards(AuthGuard(),RolesGuard,BlacklistGuard)
  @UseFilters(CustomerNotFoundFilter)
  @Roles('admin')
  async getOutlet(@Param('customerId') customerId: string): Promise<ShowCustomerDTO> {
    const foundedCustomer = await this.customersService.getCustomer(customerId);
    console.log(foundedCustomer);
    return foundedCustomer;
  }

  @Get('')
  @UseGuards(AuthGuard(),RolesGuard,BlacklistGuard)
  @Roles('admin')
  async getAllOutlets(): Promise<ShowCustomerDTO[]> {
    const foundedCustomers = await this.customersService.getAll();
    console.log(foundedCustomers);
    return foundedCustomers;
  }

  @Put('/:customerId')
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles('admin')
  async updateCustomer(@Param('customerId') customerId: string, @Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) name: UpdateCustomerDTO, ): Promise<ShowCustomerDTO> {
    return await this.customersService.updateCustomer(customerId, name);
  }

}
