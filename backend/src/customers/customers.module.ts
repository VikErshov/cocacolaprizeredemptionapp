import { Module } from '@nestjs/common';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { CustomersController } from './customers.controller';

@Module({
    imports: [
        CoreModule,
        AuthModule],
      controllers: [CustomersController],
})
export class CustomersModule {}
