import { PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, Entity } from 'typeorm';
import { User } from './user';
import { activityEnum } from '../../common/enums/activity.enum';

@Entity('activity')
export class Activity {
     @PrimaryGeneratedColumn('uuid')
     id: string;

     @CreateDateColumn()
     date: Date;

     @ManyToOne(type => User, user => user.activity)
     user: Promise<User>;

     @Column({
          type: 'enum',
          enum: activityEnum,
          default: null,
        })
        activityType: activityEnum;

}
