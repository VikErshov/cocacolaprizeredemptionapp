import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, OneToMany, ManyToOne } from "typeorm";
import { RedemptionRecord } from "./prizeRedemption";
import { Prize } from "./prize";
import { PrizeEnum } from "../../common/enums/prize.enum";

@Entity('barcode')
export class Barcode {
     @PrimaryGeneratedColumn('uuid')
     id: string;

     @Column()
     barcode: string;

     @Column({
          type: 'boolean',
          default: false,
     })
     isRedeem: boolean;

}
