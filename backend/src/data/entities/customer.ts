import { PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, Entity } from "typeorm";
import { Outlet } from "./outlet";
import { User } from "./user";
import { RedemptionRecord } from "./prizeRedemption";
@Entity('customer')
export class Customer {
     
     @PrimaryGeneratedColumn('uuid')
     id: string;

     @Column()
     name: string;

     @OneToMany(type => Outlet, outlet => outlet.title)
     outlet: Promise<Outlet[]>;

     @Column({ default: false })
     isDeleted: boolean;

     @OneToMany(type => RedemptionRecord, redemptionRecords => redemptionRecords.customer)
     redemptionRecords: Promise<RedemptionRecord[]>;
}
