import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, OneToOne, ManyToOne } from "typeorm";
import { User } from "./user";


@Entity('message')
export class Message {
     @PrimaryGeneratedColumn('uuid')
     id: string;

     @Column()
     issueTitle: string;

     @Column()
     description: string;

     @ManyToOne(type => User, user => user.messages)
     user: Promise<User>;

     @Column({ default: false })
     isRead: boolean;

     @Column({ default: false })
     isDeleted: boolean;

}
