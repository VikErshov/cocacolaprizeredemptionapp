import { Entity, Column, OneToMany, PrimaryGeneratedColumn, ManyToMany, ManyToOne, CreateDateColumn, UpdateDateColumn, VersionColumn } from "typeorm";

import { User } from "./user";
import { Customer } from "./customer";
import { RedemptionRecord } from "./prizeRedemption";

@Entity('outlet')
export class Outlet {

     @PrimaryGeneratedColumn('uuid')
     id: string;

     @Column('nvarchar')
     title: string;

     @Column()
     location: string;

     @OneToMany(type => User, user => user.outlet)
     user: Promise<User[]>;

     @ManyToOne(type => Customer, customer => customer.outlet)
     brand: Promise<Customer>;

     @OneToMany(type => RedemptionRecord, redemptionRecords => redemptionRecords.outlet)
     redemptionRecords: Promise<RedemptionRecord[]>;

     @CreateDateColumn()
     createdOn: Date;

     @UpdateDateColumn()
     updatedOn: Date;

     @Column({ default: false })
     isDeleted: boolean;

     @VersionColumn()
     version: number;
}
