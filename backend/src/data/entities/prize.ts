import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, OneToOne } from "typeorm";
import { RedemptionRecord } from "./prizeRedemption";
import { Barcode } from "./barcode";

@Entity('prize')
export class Prize {
     @PrimaryGeneratedColumn('uuid')
     id: string;

     @Column()
     prizeType: string;

     @Column()
     quantity: number;

     @Column()
     name: string;

     @OneToMany(type => RedemptionRecord, redemptionRecord => redemptionRecord.prize, { eager: false })
     @JoinColumn()
     redemptionRecord: Promise<RedemptionRecord[]>;
}
