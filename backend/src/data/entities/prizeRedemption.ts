import { Entity, PrimaryGeneratedColumn, OneToOne, JoinColumn, ManyToOne, CreateDateColumn, UpdateDateColumn, VersionColumn } from "typeorm";
import { Barcode } from "./barcode";
import { Customer } from "./customer";
import { Outlet } from "./outlet";
import { User } from "./user";
import { Prize } from "./prize";

@Entity('redemptionRecords')
export class RedemptionRecord {
     @PrimaryGeneratedColumn('uuid')
     id: string;

     @ManyToOne(type => Barcode, barcode => barcode.id, {
          eager: false,
     })
     @JoinColumn()
     barcode: Promise<Barcode>;

     @ManyToOne(type => Outlet, outlet => outlet.redemptionRecords)
     outlet: Promise<Outlet>;

     @ManyToOne(type => User, user => user.redemptionRecords)
     user: Promise<User>;

     @ManyToOne(type => Prize, prize => prize.redemptionRecord, { eager: true })
     @JoinColumn()
     prize: Promise<Prize>;

     @CreateDateColumn()
     createdOn: Date;

     @UpdateDateColumn()
     updatedOn: Date;

     @VersionColumn()
     version: number;

     @ManyToOne(type => Customer, customer => customer.redemptionRecords)
     customer: Promise<Customer>;
     
}
