import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  VersionColumn,
  OneToOne,
  JoinColumn,
  ManyToMany,
  JoinTable,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { UserRole } from '../../common/enums/user-role.enum';
import { Outlet } from './outlet';
import { Customer } from './customer';
import { RedemptionRecord } from './prizeRedemption';
import { userInfo, type } from 'os';
import { Activity } from './activity';
import { Blacklist } from './blacklist';
import { Message } from './message';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar', {unique: true})
  name: string;

  @Column('nvarchar')
  password: string;

  @Column({ unique: true })
  email: string;

  @ManyToOne(type => Outlet, outlet => outlet.user)
  outlet: Promise<Outlet>;

  @OneToMany(type => RedemptionRecord, redemptionRecords => redemptionRecords.user)
  redemptionRecords: Promise<RedemptionRecord[]>;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @VersionColumn()
  version: number;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.USER,
  })
  roles: UserRole;

  @OneToMany(type => Message, message => message.user)
  messages: Promise<Message[]>;

  @OneToMany(type => Activity, activity => activity.user)
  activity: Promise<User>;

  @Column({ default: false })
    isDeleted: boolean;

  @OneToMany(type => Blacklist, blacklist => blacklist.user)
  blacklist: Promise<Blacklist[]>;
}
