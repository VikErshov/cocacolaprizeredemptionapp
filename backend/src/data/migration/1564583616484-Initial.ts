import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1564583616484 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `barcode` (`id` varchar(36) NOT NULL, `barcode` varchar(255) NOT NULL, `isRedeem` tinyint NOT NULL DEFAULT 0, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `prize` (`id` varchar(36) NOT NULL, `prizeType` varchar(255) NOT NULL, `quantity` int NOT NULL, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `redemptionRecords` (`id` varchar(36) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, `barcodeId` varchar(36) NULL, `outletId` varchar(36) NULL, `userId` varchar(36) NULL, `prizeId` varchar(36) NULL, `customerId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `customer` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `outlet` (`id` varchar(36) NOT NULL, `title` varchar(255) NOT NULL, `location` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `version` int NOT NULL, `brandId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `blacklists` (`id` int NOT NULL AUTO_INCREMENT, `token` varchar(255) NOT NULL, `expiresOn` varchar(255) NOT NULL, `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `message` (`id` varchar(36) NOT NULL, `issueTitle` varchar(255) NOT NULL, `description` varchar(255) NOT NULL, `isRead` tinyint NOT NULL DEFAULT 0, `isDeleted` tinyint NOT NULL DEFAULT 0, `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, `roles` enum ('user', 'admin') NOT NULL DEFAULT 'user', `isDeleted` tinyint NOT NULL DEFAULT 0, `outletId` varchar(36) NULL, UNIQUE INDEX `IDX_065d4d8f3b5adb4a08841eae3c` (`name`), UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `activities` (`id` varchar(36) NOT NULL, `description` varchar(255) NOT NULL, `date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_0dbea2509b98b01d251fb6a8786` FOREIGN KEY (`barcodeId`) REFERENCES `barcode`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_ab593268faf5e832760a6df651b` FOREIGN KEY (`outletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_1f92cd37a4f6f2531c2072bf9f3` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_be50de253e282aad9aaaaa0b663` FOREIGN KEY (`prizeId`) REFERENCES `prize`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_e5ecc2f09e94f653cf356fbc9b8` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `outlet` ADD CONSTRAINT `FK_da0a7931943e274c1f026c48729` FOREIGN KEY (`brandId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `blacklists` ADD CONSTRAINT `FK_6e4f6b4f8a6c9ceefd32b6f11e7` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `message` ADD CONSTRAINT `FK_446251f8ceb2132af01b68eb593` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_3d3b1fbedf229c3994b68a477fe` FOREIGN KEY (`outletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `activities` ADD CONSTRAINT `FK_5a2cfe6f705df945b20c1b22c71` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `activities` DROP FOREIGN KEY `FK_5a2cfe6f705df945b20c1b22c71`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_3d3b1fbedf229c3994b68a477fe`");
        await queryRunner.query("ALTER TABLE `message` DROP FOREIGN KEY `FK_446251f8ceb2132af01b68eb593`");
        await queryRunner.query("ALTER TABLE `blacklists` DROP FOREIGN KEY `FK_6e4f6b4f8a6c9ceefd32b6f11e7`");
        await queryRunner.query("ALTER TABLE `outlet` DROP FOREIGN KEY `FK_da0a7931943e274c1f026c48729`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_e5ecc2f09e94f653cf356fbc9b8`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_be50de253e282aad9aaaaa0b663`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_1f92cd37a4f6f2531c2072bf9f3`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_ab593268faf5e832760a6df651b`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_0dbea2509b98b01d251fb6a8786`");
        await queryRunner.query("DROP TABLE `activities`");
        await queryRunner.query("DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`");
        await queryRunner.query("DROP INDEX `IDX_065d4d8f3b5adb4a08841eae3c` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP TABLE `message`");
        await queryRunner.query("DROP TABLE `blacklists`");
        await queryRunner.query("DROP TABLE `outlet`");
        await queryRunner.query("DROP TABLE `customer`");
        await queryRunner.query("DROP TABLE `redemptionRecords`");
        await queryRunner.query("DROP TABLE `prize`");
        await queryRunner.query("DROP TABLE `barcode`");
    }

}
