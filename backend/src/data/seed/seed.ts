import 'reflect-metadata';
import { createConnection } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from '../entities/user';
import { Barcode } from '../entities/barcode';
import { UserRole } from '../../common/enums/user-role.enum';
import { Prize } from '../entities/prize';
import { Outlet } from '../entities/outlet';
import { Customer } from '../entities/customer';

const main = async () => {
     const connection = await createConnection();

     const userRepo = connection.manager.getRepository(User);
     const barcodeRepo = connection.manager.getRepository(Barcode);
     const prizeRepo = connection.manager.getRepository(Prize);
     const outletRepo = connection.manager.getRepository(Outlet);
     const customerRepo = connection.manager.getRepository(Customer);

     
     const prize1 = new Prize();
     prize1.name = 'Coca-Cola';
     prize1.prizeType = "piene";
     prize1.quantity = 100;
     await prizeRepo.save(prize1)

     const barcode1 = new Barcode();
     barcode1.barcode = '5060517884031';
     // barcode1.prize = Promise.resolve(await prizeRepo.findOne({where: {name: 'Coca:Cola'}}))
     await barcodeRepo.save(barcode1)

     const barcode2 = new Barcode();
     barcode2.barcode = '5449000273468';
     await barcodeRepo.save(barcode2)
     // const post11 = new Post()
     // post11.title = 'This is eleventh post';
     // post11.content = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore `;
     // post11.author = Promise.resolve(await userRepo.findOne({ where: { username: 'JohnyRuti' } }));
     // await postRepo.save(post11)

     const outlet = new Outlet();
     outlet.title = 'Kaufland';
     outlet.location = 'Aleksandar Malinov N:11';
     outlet.user[0] = Promise.resolve(await outletRepo.findOne({ where: { username: 'Sansa' } }));
     await outletRepo.save(outlet);

     const outlet2 = new Outlet();
     outlet2.title = 'Lidl';
     outlet2.location = 'Saedinenie N:7';
     outlet2.user[1] = Promise.resolve(await outletRepo.findOne({ where: { username: 'Jamie' } }));
     await outletRepo.save(outlet2);

     const customer1 = new Customer();
     customer1.name = 'Kaufland';
     await customerRepo.save(customer1);

     const John = await userRepo.findOne({
          where: {
               name: 'JohnyRuti',
          },
     });

     if (!John) {
          const user1 = new User();
          user1.name = 'JohnyRuti',
               user1.email = 'realking@starks.com',
               user1.password = await bcrypt.hash('thisIsTest6', 10);
          user1.roles = UserRole.USER;
          await userRepo.save(user1);
          console.log(`John created`);
     } else {
          console.log(`John already in the db`);
     }

     const Dany = await userRepo.findOne({
          where: {
               name: 'MadQueen',
          },
     });

     if (!Dany) {
          const user2 = new User();
          user2.name = 'MadQueen',
               user2.roles = UserRole.USER,
               user2.email = 'realqueen@crazytown.com',
               user2.password = await bcrypt.hash('thisIsTest6', 10);
          user2.outlet = Promise.resolve(outlet);

          await userRepo.save(user2);
          console.log(`Dany created`);
     } else {
          console.log(`Dany already in the db`);
     }

     const Jamie = await userRepo.findOne({
          where: {
               name: 'KingSlayer',
          },
     });

     if (!Jamie) {
          const user3 = new User();
          user3.name = 'KingSlayer',
               user3.email = 'lovemysister@lanisters.com',
               user3.password = await bcrypt.hash('thisIsTest6', 10);
          user3.outlet = Promise.resolve(outlet);
          await userRepo.save(user3);
          console.log(`Jamie created`);
     } else {
          console.log(`Jamie already in the db`);
     }

     const Manchev = await userRepo.findOne({
          where: {
               name: 'Manchev',
          },
     });

     if (!Manchev) {
          const user4 = new User();
          user4.name = 'Manchev',
               user4.email = 'manchev@abv.bg',
               user4.password = await bcrypt.hash('C8hqyh1a@', 10);
          user4.roles = UserRole.ADMIN;

          await userRepo.save(user4);
          console.log(`Manchev created`);
     } else {
          console.log(`Manchev already in the db`);
     }

     const Tyrion = await userRepo.findOne({
          where: {
               username: 'ShortLanister',
          },
     });

     connection.close();

}
main().catch(console.error);