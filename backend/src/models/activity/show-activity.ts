import { Expose } from "class-transformer";
import { User } from "../../data/entities/user";
import { activityEnum } from "../../common/enums/activity.enum";

export class ShowActivityDTO {

     @Expose()
     date: Date;
     @Expose()
     user: User;
     @Expose()
     activityType: activityEnum;

}
