
import { Expose } from 'class-transformer';
import { ShowOutletDTO } from '../outlet/outlet-show-dto';

export class ShowCustomerDTO {

  @Expose()
  id: string;

  @Expose()
  name: string;

}
