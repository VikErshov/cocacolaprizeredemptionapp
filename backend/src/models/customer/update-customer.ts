import { Expose } from 'class-transformer';
import { IsString } from 'class-validator';

export class UpdateCustomerDTO {

     @IsString()
     @Expose()
     name: string;

}
