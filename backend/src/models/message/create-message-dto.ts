import { IsString } from "class-validator";


export class CreateMessageDTO {

     @IsString()
     issueTitle: string;

     @IsString()
     description: string;

     @IsString()
     username: string;
}
