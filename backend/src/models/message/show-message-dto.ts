import { Expose } from "class-transformer";

export class ShowMessageDTO {

    @Expose()
    id: string;

    @Expose()
    issueTitle: string;

    @Expose()
    description: string;

    @Expose()
    isRead: boolean;

}
