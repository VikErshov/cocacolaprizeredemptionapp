import { IsString } from "class-validator";
import { Expose } from "class-transformer";
import { Customer } from "../../data/entities/customer";

export class CreateOutletDTO {
     @IsString()
     title: string;

     @IsString()
     location: string;

     @Expose()
     customerId: Customer;
}