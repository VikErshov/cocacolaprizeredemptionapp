import { IsString } from "class-validator";
import { Expose } from "class-transformer";

export class ShowOutletDTO {

     @IsString()
     @Expose()
     id: string;

     @IsString()
     @Expose()
     title: string;

     @IsString()
     @Expose()
     location: string;

     @Expose()
     customerId: string;
}