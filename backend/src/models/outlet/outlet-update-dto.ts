import { IsString } from "class-validator";
import { Expose } from "class-transformer";

export class OutletUpdateDTO {
     @IsString()
     location: string;
}