import { IsString } from "class-validator";
import { Expose } from "class-transformer";

export class PrizeDTO {
     @IsString()
     id:string;

     @IsString()
     name:string;
}
