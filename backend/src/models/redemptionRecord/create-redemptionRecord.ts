import { IsString } from "class-validator";
import { Expose } from "class-transformer";

export class CreatePrizeRedemptionDTO {
     @IsString()
     barcode:string;

     @IsString()
     outlet:string;

     @IsString()
     prize:string;
}
