import { IsString, IsDate } from "class-validator";
import { Expose } from "class-transformer";

export class PrizeRedemptionDTO {
     
     @Expose()
     id:string;
     
     @Expose()
     createdOn:Date;
     
     @Expose()
     updatedOn:Date;
     
     
     @Expose()
     barcode: string;
     
     
     @Expose()
     outlet: string;

     
     @Expose()
     user:string;

     
     @Expose()
     prize:string;
     
     @Expose()
     customer:string;
     
}
