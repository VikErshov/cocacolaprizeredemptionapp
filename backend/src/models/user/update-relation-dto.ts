import { IsString } from "class-validator";

export class UpdateRelationDTO {
    @IsString()
    newRelation: string;
}
