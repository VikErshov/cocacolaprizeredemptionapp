import { IsEmail, IsString, Matches, IsOptional } from 'class-validator';
import { Expose } from 'class-transformer';
import { UserRole } from '../../common/enums/user-role.enum';

export class UpdateUserDTO {

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  password: string;

  @IsOptional()
  @IsEmail()
  email: string;

}
