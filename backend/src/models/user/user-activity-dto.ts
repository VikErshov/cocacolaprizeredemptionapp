export class ShowActivityDTO {
     id: string;
     description: string;
     date: Date;
}
