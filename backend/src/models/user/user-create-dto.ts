import { IsString, IsEmail } from "class-validator";
import { Expose } from "class-transformer";
import { Outlet } from "../../data/entities/outlet";

export class UserCreatedDTO {
     @IsString()
     name:string;

     @IsString()
     password:string;

     @IsEmail()
     email:string

     @Expose()
     outletId:Outlet;
}