import { IsEmail, IsString, Matches } from 'class-validator';
import { Expose } from 'class-transformer';
import { UserRole } from '../../common/enums/user-role.enum';

export class ShowUserDTO {
  @IsString()
  @Expose()
  id: string;

  @IsString()
  @Expose()
  name: string;

  @IsEmail()
  @Expose()
  email: string;

  @IsString()
  @Expose()
  roles: UserRole;
}
