import { Controller, Get, Param, UseGuards, UseFilters, Post, Delete, HttpCode, HttpStatus, Put, Body, ValidationPipe } from '@nestjs/common';
import { UsersService } from '../core/services/users.service';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../common/decorators/roles.decorator';
import { RolesGuard } from '../common/guards/roles.guard';
import { ShowActivityDTO } from '../models/user/user-activity-dto';
import { BlacklistGuard } from '../common/guards/blacklist.guard';
import { User } from '../data/entities/user';
import { AuthUser } from '../decorators/user.decorator';
import { OutletService } from '../core/services/outlets.service';
import { OutletUpdateDTO } from '../models/outlet/outlet-update-dto';
import { ShowOutletDTO } from '../models/outlet/outlet-show-dto';
import { CreateOutletDTO } from '../models/outlet/outlet-create-dto';
import { DuplicateEntry } from '../core/customErrors/duplicate-outlets';
import { DuplicateExceptionFilter } from '../core/filters/duplicateEntryFilter';
import { AlreadyDeletedFilter } from '../core/filters/outletIsAlreadyDeleted';
import { OutletsNotFound } from '../core/filters/outletNotFound.filter';

@Controller('')
export class OutletController {

     constructor(private readonly outletService: OutletService) { }


     @Post('/outlets')
     @UseGuards(AuthGuard(), RolesGuard, BlacklistGuard)
     @Roles('admin')
     @UseFilters(DuplicateExceptionFilter)
     async createOutlet(@Body(new ValidationPipe({
          transform: true,
          whitelist: true,
     })) outlet: CreateOutletDTO): Promise<ShowOutletDTO> {
          console.log(outlet);
          return this.outletService.createOutlet(outlet);
     }

     @Get('customers/:customerId/outlets')
     @UseGuards(AuthGuard(), RolesGuard, BlacklistGuard)
     @Roles('admin')
     async getAllfromOneBrand(@Param('customerId') customerId: string): Promise<ShowOutletDTO[]> {
          const foundedOutlets = await this.outletService.getAllfromOneBrand(customerId);

          return foundedOutlets;
     }

     @Get('customers/:customerId/outlets/:outletsId')
     @UseGuards(AuthGuard(), RolesGuard, BlacklistGuard)
     @UseFilters(OutletsNotFound)
     @Roles('admin')
     async getOutlet(@Param('customerId') customerId, @Param('outletsId') outletId: string): Promise<ShowOutletDTO> {
          const foundedOutlet = await this.outletService.getOutlet(customerId, outletId);
          console.log(foundedOutlet);
          return foundedOutlet;
     }

     @Put('customers/:customerId/outlets/:outletsId')
     @UseGuards(AuthGuard(), RolesGuard, BlacklistGuard)
     @UseFilters(OutletsNotFound)
     @Roles('admin')
     async updateOutlet(@Param('customerId') customerId: string, @Param('outletsId') outletId: string, @Body(new ValidationPipe({
          transform: true,
          whitelist: true,
     })) location: OutletUpdateDTO, ): Promise<ShowOutletDTO> {
          return await this.outletService.updateOutlet(customerId, outletId, location);
     }

     @Delete('customers/:customerId/outlets/:outletId')
     @UseGuards(AuthGuard(), RolesGuard, BlacklistGuard)
     @Roles('admin')
     @UseFilters(AlreadyDeletedFilter)
     async deleteOutlet(@Param('customerId') customerId, @Param('outletId') outletId: string): Promise<ShowOutletDTO> {

          return await this.outletService.deleteOutlet(outletId, customerId);
     }

}