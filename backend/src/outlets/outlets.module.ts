import { Module } from "@nestjs/common";
import { CoreModule } from "../core/core.module";
import { AuthModule } from "../auth/auth.module";
import { OutletController } from "./outlets.controller";

@Module({
     imports: [
          CoreModule,
          AuthModule
     ],
     controllers: [OutletController]
})

export class OutletModule {}