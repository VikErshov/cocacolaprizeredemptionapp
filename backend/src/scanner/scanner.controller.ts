import { ScannerService } from "../core/services/scanner.service";
import { Post, UseGuards, UseFilters, Body, ValidationPipe, Controller, Param, Get } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { BlacklistGuard } from "../common/guards/blacklist.guard";
import { DuplicateExceptionFilter } from "../core/filters/duplicateEntryFilter";
import { ScanBarcodeDTO } from "../models/barcode/scan-barcode-dto";
import { ShowBarcodeDTO } from "../models/barcode/show-barcode-dto";
import { PrizeRedemptionDTO } from "../models/redemptionRecord/prizeRedemption-dto";
import { PrizeEnum } from "../common/enums/prize.enum";
import { Barcode } from "../data/entities/barcode";
import { BarcodeAlreadyRedeemedFilter } from "../core/filters/barcode-alreadyRedeemedFilter";
import { BarcodeNotFoundFilter } from "../core/filters/barcodeNotFound";
import { RolesGuard } from "../common/guards/roles.guard";
import { Roles } from "../common/decorators/roles.decorator";


@Controller('')
export class ScannerController {
     
     constructor(private readonly scannerService:ScannerService){}

     @Get(':userId/scan/:barcode')
     
     @UseFilters(BarcodeNotFoundFilter)
     async createBarcode(@Param('barcode') barcode: string, @Param('userId') userId:string): Promise<any> {
          return this.scannerService.scan(barcode,userId);
     }

     @Post(':userId/scan/:barcode/redeem/:prize')
     @UseFilters(BarcodeAlreadyRedeemedFilter)
     
     async redeemBarcode( @Param('userId') userId:string,@Body(new ValidationPipe({
          transform: true,
          whitelist: true,
     })) barcode: ScanBarcodeDTO, @Param('prize') prize:string): Promise<any> {
          return this.scannerService.redeemPrize(userId,barcode,prize);
     }

     @Post('/barcode')
     @UseGuards(AuthGuard(),RolesGuard)
     @Roles('admin')
     async barcode(@Body(new ValidationPipe({
          transform: true,
          whitelist: true,
     })) barcode: ScanBarcodeDTO): Promise<ScanBarcodeDTO> {
          return this.scannerService.barcode(barcode);
     }

     @Get('prizes')
     async getPrizes():Promise<any>{
          return this.scannerService.getPrizes();
     }

     @Get('/:barcode')
     async redeem(@Param('barcode') barcode:string): Promise<any> {
          return this.scannerService.isRedeem(barcode);
     }
}
