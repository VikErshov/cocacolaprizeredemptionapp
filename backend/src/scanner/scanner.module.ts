import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CoreModule } from "../core/core.module";
import { Prize } from "../data/entities/prize";
import { Barcode } from "../data/entities/barcode";
import { Blacklist } from "../data/entities/blacklist";
import { RedemptionRecord } from "../data/entities/prizeRedemption";
import { User } from "../data/entities/user";
import { ScannerController } from "./scanner.controller";
import { AuthModule } from "../auth/auth.module";
import { Outlet } from "../data/entities/outlet";

@Module({
     imports: [
          TypeOrmModule.forFeature([Prize, Barcode, Outlet, RedemptionRecord, User]),
          CoreModule,

     ],
     controllers: [ScannerController]
})

export class ScannerModule { }
