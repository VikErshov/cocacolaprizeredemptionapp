import { Controller, Get, Param, UseGuards, UseFilters, Post, Delete, Put, ValidationPipe, Body, UnauthorizedException, HttpCode, HttpStatus } from '@nestjs/common';
import { UsersService } from '../core/services/users.service';
import { AuthGuard } from '@nestjs/passport';
import { ShowUserDTO } from '../models/user/user-show-dto';
import { UserIsDeletedFilter } from '../core/filters/userIsDeleted.filter';
import { UserNotFoundFilter } from '../core/filters/userNotFound.filter';
import { Roles } from '../common/decorators/roles.decorator';
import { RolesGuard } from '../common/guards/roles.guard';
import { UpdateUserDTO } from '../models/user/update-user-dto';
import { BlacklistGuard } from '../common/guards/blacklist.guard';
import { AuthUser } from '../decorators/user.decorator';
import { User } from '../data/entities/user';
import { ShowActivityDTO } from '../models/user/user-activity-dto';
import { UserRegisterDTO } from '../models/user/user-register-dto';
import { UserCreatedDTO } from '../models/user/user-create-dto';
import { DuplicateExceptionFilter } from '../core/filters/duplicateEntryFilter';
import { UpdateRelationDTO } from '../models/user/update-relation-dto';
import { CreateMessageDTO } from '../models/message/create-message-dto';
import { ShowMessageDTO } from '../models/message/show-message-dto';
import { RedemptionRecord } from '../data/entities/prizeRedemption';
import { PrizeRedemptionDTO } from '../models/redemptionRecord/prizeRedemption-dto';

@Controller('customers')
export class UsersController {

  constructor(private readonly usersService: UsersService) { }

  @Post(':customerId/outlets/:outletId/user')
  @UseGuards(AuthGuard(), RolesGuard)
  @UseFilters(UserNotFoundFilter, DuplicateExceptionFilter)
  @Roles('admin')
  public async createUser(@Body(new ValidationPipe(
    {
      transform: true,
      whitelist: true,
    })) user: UserCreatedDTO, @Param('outletId') outletId: string): Promise<ShowUserDTO> {
    return await this.usersService.createUser(user, outletId);
  }

  @Post('users/messages')
  @UseGuards(AuthGuard(), RolesGuard)
  @UseFilters(UserNotFoundFilter)
  @Roles('user', 'admin')
  public async createMessage(@Body(new ValidationPipe(
    {
      transform: true,
      whitelist: true,
    })) message: CreateMessageDTO): Promise<ShowMessageDTO> {
    return await this.usersService.createMessage(message);
  }

  @Get('users/messages')
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles('admin')
  public async GetAllMessages(): Promise<ShowMessageDTO[]> {
    return await this.usersService.getMessagesForAll();
  }

  @Delete('users/messages/readmessage/:messageId')
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles('admin')
  public async readMessage(@Param('messageId') messageId: string): Promise<ShowMessageDTO> {
    return await this.usersService.readMessage(messageId);
  }

  @Delete('users/messages/unreadmessage/:messageId')
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles('admin')
  public async unreadMessage(@Param('messageId') messageId: string): Promise<ShowMessageDTO> {
    return await this.usersService.unreadMessage(messageId);
  }

  @Delete('users/messages/deletemessage/:messageId')
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles('admin')
  public async deleteMessage(@Param('messageId') messageId: string): Promise<ShowMessageDTO> {
    return await this.usersService.deleteMessage(messageId);
  }

  @Get(':customerId/outlets/:outletId/user')
  @UseGuards(AuthGuard(), RolesGuard)
  @UseFilters(UserNotFoundFilter)
  @Roles('admin')
  public async GetAllUsersFromOneBrand(@Param('customerId') customerId: string, @Param('outletId') outletId: string): Promise<ShowUserDTO[]> {
    return await this.usersService.getAllFromOneOutlet(customerId, outletId);
  }

  @Get(':customerId/outlets/:outletsId/user/:userId')
  @UseGuards(AuthGuard())
  @UseFilters(UserNotFoundFilter)
  public async GetSpecificUsers(@Param('customerId') customerId: string, @Param('outletsId') outletId: string, @Param('userId') id: string): Promise<ShowUserDTO> {
    return await this.usersService.getUserById(customerId, outletId, id);
  }

  @Get('/user')
  @UseGuards(AuthGuard())
  @UseFilters(UserNotFoundFilter)
  public async getByUsername(@Param('username') username: string): Promise<ShowUserDTO> {
    return await this.usersService.findUserByName(username);
  }

  @Put(':customerId/outlets/:outletsId/user/:userId')
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles('admin')
  @UseFilters(UserNotFoundFilter, UserIsDeletedFilter)
  public async UpdateUser(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) updatedUser: UpdateUserDTO,
    @Param('userId') userId: string): Promise<ShowUserDTO> {

    return await this.usersService.updateSpecificUser(updatedUser, userId);
  }

  @Put('/:customersId/outlets/:outletId/:userId/changerelation')
  @UseGuards(AuthGuard(), RolesGuard)
  @Roles('admin')
  @UseFilters(UserNotFoundFilter, UserIsDeletedFilter)
  public async ChangeRelation(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) updateRelationDTO: UpdateRelationDTO,
    @Param('userId') userId: string,
    @Param('newOutletId') newOutletId: string): Promise<ShowUserDTO> {

    return await this.usersService.changeRelation(updateRelationDTO, userId);
  }

  @Delete(':customerId/outlets/:outletsId/user/:userId')
  @UseGuards(AuthGuard(), RolesGuard)
  @UseFilters(UserNotFoundFilter, UserIsDeletedFilter)
  @Roles('admin')
  public async DeleteUser(@Param('userId') id): Promise<ShowUserDTO> {
    return await this.usersService.deleteUser(id);
  }

  // @Get('/activity/:id')
  // @HttpCode(HttpStatus.OK)
  // @UseGuards(AuthGuard(), BlacklistGuard, RolesGuard)
  // @Roles('admin')
  // public async getActivity(@Param('id') id: string, @AuthUser() user: User): Promise<ShowActivityDTO[]> {
  //   return await this.usersService.getActivity(id, user);
  // }

  @Get('redemption-records/:userId')
  @HttpCode(HttpStatus.OK)
  @UseGuards()
  // @Roles('')
  public async getRedemptionRecordsByUser(@Param('userId') userId: string, @AuthUser() user: User): Promise<PrizeRedemptionDTO[]> {
    return await this.usersService.getRedemptionRecordByUser(userId);
  }

  
}
