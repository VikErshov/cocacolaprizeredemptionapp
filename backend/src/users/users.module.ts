import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './users.controller';
import { User } from '../data/entities/user';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { Activity } from '../data/entities/activity';
import { Blacklist } from '../data/entities/blacklist';
import { Message } from '../data/entities/message';

@Module({
    imports: [
        TypeOrmModule.forFeature([User, Activity, Blacklist, Message]),
        CoreModule,
        AuthModule],
      controllers: [UsersController],
})
export class UsersModule {}
