### Frontend ###

** Go to the backend folder and run "npm run start:dev" to start the Nest JS backend part. **
** Go to the frontend folder and run "ng s" to start the Angular frontend part.

# Admin functionalities


Log in with an admin account and you'll be automatically routed to "Manage customers". 
You can perform all CRUD operations on customers as well as sort by clicking on the little arrows and filter by typing in the search bar. 
Click on  a customer in order to get to its outlets. Same functionalities as above.
Click on an outlet and you get to all its users. Same functionalities as above.
When you navigate to a specific user, in the left tab you'll see basic user info and a button, by clicking on which, a dropdown with all outlets of the current customer appears and you are able to move the user to a different outlet if you want to.
On the right tab, you can see detailed information regarding redemption records made by the current user.


Click on "Navigate" and on "Messages". You'll be navigated to a page, where you can see all issue tickets and mark them as "read" or "unread" as you see fit, as well as delete them.

# User functionalities

Log in as a basic user and you'll be able redirected to the user panel. Click on "scan barcode" and you'll be redirected to another page where your camera will open. Scan a barcode and if it is valid, you'll be prompted to choose a prize.
Same applies for inputting the barcode manually, a modal window pops up and you type it in.

If any issue occurs, click on "Help desk" to submit a ticket, which will be received by the admin.
