import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { RolesGuard } from 'app/auth/roles.guard';
import { MessagesComponent } from './messages/messages.component';
import { MessageResolverService } from './services/message-resolver.service';


const routes: Routes = [

  {path: '', component: MessagesComponent,
  resolve: {messages: MessageResolverService},
  canActivate: [AuthGuard, RolesGuard],
  data: {
    expectedRole: 'admin'
  },
  pathMatch: 'full'},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminSecondaryRoutingModule { }
