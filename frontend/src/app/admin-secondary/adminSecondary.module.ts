import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminSecondaryRoutingModule } from './adminSecondary-routing.module';
import { MessagesComponent } from './messages/messages.component';

@NgModule({
  declarations: [
    MessagesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    AdminSecondaryRoutingModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  entryComponents: [

 ],
})
export class AdminSecondaryModule { }
