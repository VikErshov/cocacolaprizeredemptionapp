export interface Message{
    id: string;
    issueTitle: string;
    description: string;
    isRead: boolean;
}