import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class MessagesService {


  constructor(
    private readonly http: HttpClient,
    ) {}

  public allMessages(): Observable<{messages: Message[]}> {
    return this.http.get<{messages: Message[]}>(`http://localhost:3000/customers/users/messages`);
  }

  public createMessage(issueTitle: string, description: string, username: string): Observable<{messages: Message}> {
    return this.http.post<{messages: Message}>('http://localhost:3000/customers/users/messages', {
      issueTitle,
      description,
      username,
    });
  }

  public readMessage(messageId: string): Observable<{messages: Message}> {
    return this.http.delete<{messages: Message}>(`http://localhost:3000/customers/users/messages/readmessage/${messageId}`);
  }

  public unreadMessage(messageId: string): Observable<{messages: Message}> {
    return this.http.delete<{messages: Message}>(`http://localhost:3000/customers/users/messages/unreadmessage/${messageId}`);
  }

  public deleteMessage(messageId: string): Observable<{messages: Message}> {
    return this.http.delete<{messages: Message}>(`http://localhost:3000/customers/users/messages/deletemessage/${messageId}`);
  }
}
