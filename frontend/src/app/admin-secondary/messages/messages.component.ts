import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessagesService } from '../message.service';
import { ActivatedRoute } from '@angular/router';
import { NotificatorService } from 'app/core/services/notificator.service';
import { Message } from '../interfaces/message';
import { Observable, of, Subscription } from 'rxjs';
import { SharedInfoService } from 'app/core/services/sharedInfo.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit, OnDestroy {

  public messages;
  public currentJustify = 'justified';
  public read;
  public unread;


  constructor(private readonly messagesService: MessagesService,
    private readonly router: ActivatedRoute,
    private readonly notificator: NotificatorService,
    private readonly sharedInfoService: SharedInfoService) { }


  ngOnInit() {
    this.router.data.subscribe(
      (result) => {
        this.messages = result.messages;
        this.read = this.messages.filter((el: Message) => el.isRead === true);
        this.unread = this.messages.filter((el: Message) => el.isRead === false);
      }
    );
  }

  public markAsRead(messageId: string) {
    console.log(messageId);
    this.messagesService.readMessage(messageId).subscribe(
      (result) => {
        const index = this.unread.findIndex(message => message.id === messageId);
        this.unread.splice(index, 1);
        this.read.push(result);
      }
    );
  }

  public unmarkAsRead(messageId: string) {
    console.log(messageId);
    this.messagesService.unreadMessage(messageId).subscribe(
      (result) => {
        const index = this.read.findIndex(message => message.id === messageId);
        this.read.splice(index, 1);
        this.unread.push(result);
      }
    );
  }

  public deleteMessage(messageId: string, typeOfMessage: string) {
    this.messagesService.deleteMessage(messageId).subscribe(
      () => {
        if (typeOfMessage === 'read') {
          const index = this.read.findIndex(message => message.id === messageId);
          this.read.splice(index, 1);
        } else {
          const index = this.unread.findIndex(message => message.id === messageId);
          this.unread.splice(index, 1);
        }
      }
    );
  }

  ngOnDestroy() {

  }

}
