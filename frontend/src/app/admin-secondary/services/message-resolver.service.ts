import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';
import { MessagesService } from '../message.service';
import { Message } from '../interfaces/message';

@Injectable({
  providedIn: 'root'
})
export class MessageResolverService implements Resolve<{messages: Message[]}> {

  constructor(
    private readonly messageService: MessagesService,
    private readonly notificator: NotificatorService,
    ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    return this.messageService.allMessages()
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          // Alternativle, if the res.error.code === 401, you can logout the user and redirect to /home
          return of(res);
        }
      ));
  }
}
