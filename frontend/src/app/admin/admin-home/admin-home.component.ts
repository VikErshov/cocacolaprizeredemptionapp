import { Component, OnInit, PipeTransform, ViewChildren, Directive, Input, Output, EventEmitter, QueryList } from '@angular/core';
import { CustomerService } from '../service';
import { Customer } from '../interfaces/customer';
import { FormControl } from '@angular/forms';
import { Observable, pipe, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NotificatorService } from 'app/core/services/notificator.service';
import { NgbdSortableHeader } from './ngbd-sortable-header.directive';
import { SortEvent } from '../interfaces/sort-event.interface';
import { compare } from '../interfaces/sort-compare';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateCustomerComponent } from '../customer/updateCustomer/updateCustomer.component';
import { ActivatedRoute } from '@angular/router';

function search(text: string, customers): Customer[] {
  return customers.filter(customer => {
    const term = text.toLowerCase();
    return customer.name.toLowerCase().includes(term);
  });
}

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {

  customers$: Observable<Customer[]>;
  filter = new FormControl('');
  public submit = false;

  constructor(private readonly service: CustomerService,
              private readonly notificator: NotificatorService,
              private readonly modalService: NgbModal,
              private readonly route: ActivatedRoute) {
  }

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.customers$.subscribe(
      result => {
        if (direction === '') {
          this.customers$ = of(result);
        } else {
          this.customers$ = of([...result].sort((a, b) => {
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
          }));
        }
      }
    );
  }

  openCreateCustomer() {
    this.submit = true;
  }

  passBack(name: string) {
    this.service.createCustomer(name).subscribe(
      (res) => {
        this.service.allCustomers().subscribe(success => {
          this.customers$ = this.filter.valueChanges.pipe(
            startWith(''),
            map(text => search(text, success))
          );
        });
        this.notificator.success('You have successfully created a customer!');
      }
    );
    this.submit = false;
  }

  public UpdateCustomer(id: string) {
    const modalRef = this.modalService.open(UpdateCustomerComponent, { size: 'sm', centered: true });
    modalRef.result.then((value) => {
      this.service.updateCustomer(id, value.name).subscribe(
        () => {
          this.service.allCustomers().subscribe(success => {
            this.customers$ = this.filter.valueChanges.pipe(
              startWith(''),
              map(text => search(text, success))
            );
          });
          this.notificator.success('You have successfully updated the customer!');
        }
      );
    }).catch((res) => console.log(res));
  }

  public DeleteCustomer(id: string) {
    this.service.deleteCustomer(id).subscribe(
      () => {
        this.service.allCustomers().subscribe(success => {
          this.customers$ = this.filter.valueChanges.pipe(
            startWith(''),
            map(text => search(text, success))
          );
        });
        this.notificator.success('You have successfully deleted the customer!');
      }
    );
  }

  ngOnInit() {
    this.route.data.subscribe(
      result => {
        this.customers$ = this.filter.valueChanges.pipe(
          startWith(''),
          map(text => search(text, result.customers))
        );
      }
    );
  }
}
