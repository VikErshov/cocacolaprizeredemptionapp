import { Directive, Input, Output, EventEmitter } from '@angular/core';
import { SortEvent } from '../interfaces/sort-event.interface';
import { SortDirection } from '../interfaces/sort-direction';

@Directive({
    selector: 'th[sortable]',
    host: {
      '[class.asc]': 'direction === "asc"',
      '[class.desc]': 'direction === "desc"',
      '(click)': 'rotate()'
    }
  })
  export class NgbdSortableHeader {

    @Input() sortable: string;
    @Input() direction: SortDirection = '';
    @Output() sort = new EventEmitter<SortEvent>();

    public rotateDirection: {[key: string]: SortDirection} = { 'asc': 'desc', 'desc': '', '': 'asc' };

    rotate() {
      this.direction = this.rotateDirection[this.direction];
      this.sort.emit({column: this.sortable, direction: this.direction});
    }
  }
