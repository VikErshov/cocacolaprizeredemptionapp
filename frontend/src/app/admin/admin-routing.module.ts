import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { RolesGuard } from 'app/auth/roles.guard';
import { CustomersResolverService } from './services/customer-resolver.service';
import { OutletHomeComponent } from './outlet-home/outlet-home.component';
import { OutletsResolverService } from './services/outlets-resolver.service';
import { UserHomeComponent } from './user-home/user-home.component';
import { IndividualUserPageComponent } from './user-individual/individual-user-page/individual-user-page.component';
import { IndividualUserResolverService } from './services/individual-user-resolver.service';

const routes: Routes = [

  {path: '', component: AdminHomeComponent,
  resolve: {customers: CustomersResolverService},
  canActivate: [AuthGuard, RolesGuard],
  data: {
    expectedRole: 'admin'
  },
  pathMatch: 'full'},

  {path: ':id', component: OutletHomeComponent,
  resolve: {outlets: OutletsResolverService},
   canActivate: [AuthGuard, RolesGuard],
   data: {
     expectedRole: 'admin'
   },
   },
   {path: ':id/outlets/:usersOfOutletId', component: UserHomeComponent,
   canActivate: [AuthGuard, RolesGuard],
   data: {
     expectedRole: 'admin',
   },

   },
   {path: ':id/outlets/:usersOfOutletId/users/:singleUserId', component: IndividualUserPageComponent,
   resolve: {user: IndividualUserResolverService, outlets: OutletsResolverService},
   canActivate: [AuthGuard, RolesGuard],
   data: {
     expectedRole: 'admin',
   },

   },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
