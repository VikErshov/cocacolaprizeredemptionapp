import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminRoutingModule } from './admin-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbdSortableHeader } from './admin-home/ngbd-sortable-header.directive';
import { CreateCustomerComponent } from './customer/createCustomer/createCustomer.component';
import { UpdateCustomerComponent } from './customer/updateCustomer/updateCustomer.component';
import { OutletHomeComponent } from './outlet-home/outlet-home.component';
import { UpdateOutletComponent } from './outlet/updateOutlet/updateOutlet.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { CreateUserComponent } from './user/createUser/createUser.component';
import { UpdateUserComponent } from './user/updateUser/updateUser.component';
import { IndividualUserPageComponent } from './user-individual/individual-user-page/individual-user-page.component';
import { DropdownComponent } from './user-individual/individual-user-page/dropdown/dropdown.component';



@NgModule({
  declarations: [
    AdminHomeComponent,
    NgbdSortableHeader,
    CreateCustomerComponent,
    UpdateCustomerComponent,
    OutletHomeComponent,
    UpdateOutletComponent,
    UserHomeComponent,
    CreateUserComponent,
    UpdateUserComponent,
    IndividualUserPageComponent,
    DropdownComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  entryComponents: [
    CreateCustomerComponent,
    UpdateCustomerComponent,
    UpdateOutletComponent,
    CreateUserComponent,
    UpdateUserComponent,
 ],
})
export class AdminModule { }
