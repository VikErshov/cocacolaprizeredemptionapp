import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-createCustomer',
  templateUrl: './createCustomer.component.html',
  styleUrls: ['./createCustomer.component.scss']
})
export class CreateCustomerComponent implements OnInit {

  constructor(private readonly activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  passBack(name: string) {

    this.activeModal.close({name});
  }

}
