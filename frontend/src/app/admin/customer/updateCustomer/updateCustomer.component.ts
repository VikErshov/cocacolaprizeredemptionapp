import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-updateCustomer',
  templateUrl: './updateCustomer.component.html',
  styleUrls: ['./updateCustomer.component.scss']
})
export class UpdateCustomerComponent {
  
  constructor(private readonly activeModal: NgbActiveModal) { }

  passBack(name: string) {

    this.activeModal.close({name});
  }
}
