export interface Outlet {
    id: string;
    title: string;
    location: string;
  }
