export interface RedemptionRecords {
     id: string;

     createdOn: Date;

     barcode: string;

     outlet: string;

     user: string;

     prize: string;

     customer: string;
} 