import { Component, OnInit, PipeTransform, ViewChildren, Directive, Input, Output, EventEmitter, QueryList } from '@angular/core';
import { CustomerService } from '../service';
import { Customer } from '../interfaces/customer';
import { FormControl } from '@angular/forms';
import { Observable, pipe, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NotificatorService } from 'app/core/services/notificator.service';
import { SortEvent } from '../interfaces/sort-event.interface';
import { compare } from '../interfaces/sort-compare';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateCustomerComponent } from '../customer/updateCustomer/updateCustomer.component';
import { ActivatedRoute } from '@angular/router';
import { NgbdSortableHeader } from '../admin-home/ngbd-sortable-header.directive';
import { Outlet } from '../interfaces/outlet';
import { OutletsService } from '../outlet.service';
import { UpdateOutletComponent } from '../outlet/updateOutlet/updateOutlet.component';

function search(text: string, outlets): Outlet[] {
  return outlets.filter(outlet => {
    const term = text.toLowerCase();
    return outlet.location.toLowerCase().includes(term);
  });
}

@Component({
  selector: 'app-outlet-home',
  templateUrl: './outlet-home.component.html',
  styleUrls: ['./outlet-home.component.scss']
})
export class OutletHomeComponent implements OnInit {

  outlets$: Observable<Outlet[]>;
  filter = new FormControl('');
  public submit = false;
  public customerId = this.route.snapshot.params['id'];
  public customer;


  constructor(private readonly service: CustomerService,
              private readonly notificator: NotificatorService,
              private readonly modalService: NgbModal,
              private readonly route: ActivatedRoute,
              private readonly outletService: OutletsService) {
  }

  


  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.outlets$.subscribe(
      result => {
        if (direction === '') {
          this.outlets$ = of(result);
        } else {
          this.outlets$ = of([...result].sort((a, b) => {
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
          }));
        }
      }
    );
  }

  openCreateOutlet() {
    this.submit = true;
  }

  passBack(location: string) {
    this.outletService.createOutlet(this.customer.name, location).subscribe(
      (res) => {
        this.outletService.allOutletsOfACustomer(this.customerId).subscribe(success => {
          this.outlets$ = this.filter.valueChanges.pipe(
            startWith(''),
            map(text => search(text, success))
          );
        });
        this.notificator.success('You have successfully created a customer!');
      }
    );
    this.submit = false;
  }

  public UpdateOutlet(outletId: string) {

    const modalRef = this.modalService.open(UpdateOutletComponent, { size: 'sm', centered: true });
    modalRef.result.then((value) => {
      console.log(value.location);
      this.outletService.updateOutlet(this.customerId, outletId, value.location).subscribe(
        () => {
          this.outletService.allOutletsOfACustomer(this.customerId).subscribe(success => {
            this.outlets$ = this.filter.valueChanges.pipe(
              startWith(''),
              map(text => search(text, success))
            );
          });
          this.notificator.success('You have successfully updated the outlet!');
        }
      );
    }).catch((res) => console.log(res));
  }

  public DeleteOutlet(outletId: string) {
    this.outletService.deleteOutlet(this.customerId, outletId).subscribe(
      () => {
        this.outletService.allOutletsOfACustomer(this.customerId).subscribe(success => {
          this.outlets$ = this.filter.valueChanges.pipe(
            startWith(''),
            map(text => search(text, success))
          );
        });
        this.notificator.success('You have successfully deleted the outlet!');
      }
    );
  }

  ngOnInit() {
    this.route.data.subscribe(
      result => {
        this.outlets$ = this.filter.valueChanges.pipe(
          startWith(''),
          map(text => search(text, result.outlets))
        );
      }
    );

    this.service.idividualCustomer(this.customerId).subscribe(
      (result) => {
        this.customer = result;
      }
    );

  }
}
