import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Outlet } from './interfaces/outlet';
import { Title } from '@angular/platform-browser';



@Injectable({
  providedIn: 'root'
})
export class OutletsService {


  constructor(
    private readonly http: HttpClient,
    ) {}

  public allOutletsOfACustomer(customerId: string): Observable<{outlets: Outlet[]}> {
    return this.http.get<{outlets: Outlet[]}>(`http://localhost:3000/customers/${customerId}/outlets`);
  }

//   public idividualCustomer(customerId: string): Observable<{customer: Customer}> {
//     return this.http.get<{customer: Customer}>(`http://localhost:3000/customers/${customerId}/outlets`);
//   }

  public createOutlet(title, location): Observable<{id, title, location}> {
    return this.http.post<{id, title, location}>('http://localhost:3000/outlets', {
      title,
      location
    });
  }

  public updateOutlet(customerId: string, outletId: string, location: string): Observable<{location}> {
    return this.http.put<{location}>(`http://localhost:3000/customers/${customerId}/outlets/${outletId}`, {
      location
    });
  }

  public deleteOutlet(customerId: string, outletId: string): Observable<{id, location}> {
    return this.http.delete<{id, location}>(`http://localhost:3000/customers/${customerId}/outlets/${outletId}`);
  }

}
