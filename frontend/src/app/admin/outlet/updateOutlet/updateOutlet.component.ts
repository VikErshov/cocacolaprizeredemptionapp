import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-updateOutlet',
  templateUrl: './updateOutlet.component.html',
  styleUrls: ['./updateOutlet.component.css']
})
export class UpdateOutletComponent {

  constructor(private readonly activeModal: NgbActiveModal) { }

  passBack(location: string) {

    this.activeModal.close({location});
  }
}

