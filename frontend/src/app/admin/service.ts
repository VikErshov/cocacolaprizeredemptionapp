import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Customer } from './interfaces/customer';



@Injectable({
  providedIn: 'root'
})
export class CustomerService {


  constructor(
    private readonly http: HttpClient,
    ) {}

  public allCustomers(): Observable<{customers: Customer[]}> {
    return this.http.get<{customers: Customer[]}>('http://localhost:3000/customers');
  }

  public idividualCustomer(customerId: string): Observable<{customer: Customer}> {
    return this.http.get<{customer: Customer}>(`http://localhost:3000/customers/${customerId}`);
  }

  public createCustomer(name: string): Observable<{id, name}> {
    return this.http.post<{id, name}>('http://localhost:3000/customers', {
      name
    });
  }

  public updateCustomer(customerId: string, name: string): Observable<{name}> {
    return this.http.put<{name}>(`http://localhost:3000/customers/${customerId}`, {
      name
    });
  }

  public deleteCustomer(customerId: string): Observable<{customer: Customer}> {
    return this.http.delete<{customer: Customer}>(`http://localhost:3000/customers/${customerId}`);
  }

}
