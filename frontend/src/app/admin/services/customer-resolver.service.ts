import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';
import { Customer } from '../interfaces/customer';
import { CustomerService } from '../service';

@Injectable({
  providedIn: 'root'
})
export class CustomersResolverService implements Resolve<{customers: Customer[]}> {

  constructor(
    private readonly customersService: CustomerService,
    private readonly notificator: NotificatorService,
    ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    return this.customersService.allCustomers()
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          return of({customers: []});
        }
      ));
  }
}
