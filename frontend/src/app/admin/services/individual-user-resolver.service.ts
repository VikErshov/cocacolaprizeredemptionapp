import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';
import { User } from '../interfaces/user';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class IndividualUserResolverService implements Resolve<{user: User} | any> {

  constructor(
    private readonly usersService: UserService,
    private readonly notificator: NotificatorService,
    ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const customerId = route.params['id'];
    const outletId = route.params['usersOfOutletId'];
    const userId = route.params['singleUserId'];
    
    return this.usersService.individualUser(customerId, outletId, userId)
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          // Alternativle, if the res.error.code === 401, you can logout the user and redirect to /home
          return of(res);
        }
      ));
  }
}
