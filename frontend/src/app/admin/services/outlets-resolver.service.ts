import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';
import { Outlet } from '../interfaces/outlet';
import { OutletsService } from '../outlet.service';
@Injectable({
  providedIn: 'root'
})
export class OutletsResolverService implements Resolve<{outlets: Outlet[]}> {

  constructor(
    private readonly outletsService: OutletsService,
    private readonly notificator: NotificatorService,
    ) { }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const id = route.params['id'];
    return this.outletsService.allOutletsOfACustomer(id)
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          // Alternativle, if the res.error.code === 401, you can logout the user and redirect to /home
          return of(res);
        }
      ));
  }
}
