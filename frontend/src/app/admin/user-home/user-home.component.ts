import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { User } from '../interfaces/user';
import { Observable, of } from 'rxjs';
import { FormControl } from '@angular/forms';
import { NgbdSortableHeader } from '../admin-home/ngbd-sortable-header.directive';
import { SortEvent } from '../interfaces/sort-event.interface';
import { compare } from '../interfaces/sort-compare';
import { CustomerService } from '../service';
import { NotificatorService } from 'app/core/services/notificator.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { OutletsService } from '../outlet.service';
import { UserService } from '../user.service';
import { startWith, map } from 'rxjs/operators';
import { CreateCustomerComponent } from '../customer/createCustomer/createCustomer.component';
import { CreateUserComponent } from '../user/createUser/createUser.component';
import { UpdateUserComponent } from '../user/updateUser/updateUser.component';

function search(text: string, users): User[] {
  return users.filter(user => {
    const term = text.toLowerCase();
    return user.name.toLowerCase().includes(term)
    || user.email.toLowerCase().includes(term);
  });
}

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss']
})
export class UserHomeComponent implements OnInit {

  users$: Observable<User[]>;
  filter = new FormControl('');
  public customerId = this.route.snapshot.params['id'];
  public customer;
  public outletId = this.route.snapshot.params['usersOfOutletId'];


  constructor(private readonly service: CustomerService,
              private readonly notificator: NotificatorService,
              private readonly modalService: NgbModal,
              private readonly route: ActivatedRoute,
              private readonly outletService: OutletsService,
              private readonly userService: UserService) {
  }


  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.users$.subscribe(
      result => {
        if (direction === '') {
          this.users$ = of(result);
        } else {
          this.users$ = of([...result].sort((a, b) => {
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
          }));
        }
      }
    );
  }

    openCreateUser() {
      const modalRef = this.modalService.open(CreateUserComponent, { size: 'lg', centered: true });
      modalRef.result.then((value) => {
        this.userService.createUser(this.customerId, this.outletId, value.name, value.email, value.password).subscribe(
          (res) => {
            this.userService.AllUsersOfAnOutlet(this.customerId, this.outletId).subscribe(success => {
              this.users$ = this.filter.valueChanges.pipe(
                startWith(''),
                map(text => search(text, success))
              );
            });
            this.notificator.success('You have successfully created a user!');
          }
        );
      });
    }

  public UpdateUser(userId: string, user) {

    const modalRef = this.modalService.open(UpdateUserComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.user = user;
    modalRef.result.then((value) => {
      this.userService.updateUser(this.customerId, this.outletId, userId, value.name, value.email, value.password).subscribe(
        () => {
          this.userService.AllUsersOfAnOutlet(this.customerId, this.outletId).subscribe(success => {
            this.users$ = this.filter.valueChanges.pipe(
              startWith(''),
              map(text => search(text, success))
            );
          });
          this.notificator.success('You have successfully updated the user!');
        }
      );
    }).catch((res) => console.log(res));
  }

  public DeleteUser(userId: string) {
    this.userService.deleteUser(this.customerId, this.outletId, userId).subscribe(
      () => {
        this.userService.AllUsersOfAnOutlet(this.customerId, this.outletId).subscribe(success => {
          this.users$ = this.filter.valueChanges.pipe(
            startWith(''),
            map(text => search(text, success))
          );
        });
        this.notificator.success('You have successfully deleted the user!');
      }
    );
  }

  ngOnInit() {
    this.userService.AllUsersOfAnOutlet(this.customerId, this.outletId).subscribe(
      result => {
        this.users$ = this.filter.valueChanges.pipe(
          startWith(''),
          map(text => search(text, result))
        );
      }
    );
  }
}
