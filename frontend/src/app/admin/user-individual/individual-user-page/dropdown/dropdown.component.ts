import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {

  @Input()
  public outlets;
  

  constructor() { }

  ngOnInit() {
    console.log(`This is it: ${this.outlets}`);
    
  }

}
