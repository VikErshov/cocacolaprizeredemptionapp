import { Component, OnInit, OnDestroy, ViewChildren, QueryList } from '@angular/core';
import { UserService } from 'app/admin/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'app/admin/interfaces/user';
import { OutletsService } from 'app/admin/outlet.service';
import { Subscription, Observable, of } from 'rxjs';
import { NotificatorService } from 'app/core/services/notificator.service';
import { RedemptionRecords } from 'app/admin/interfaces/redemptionRecords';
import { text } from '@angular/core/src/render3/instructions';
import { startWith, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { NgbdSortableHeader } from 'app/admin/admin-home/ngbd-sortable-header.directive';
import { SortEvent } from 'app/admin/interfaces/sort-event.interface';
import { compare } from 'app/admin/interfaces/sort-compare';

function search(text: string, redemptionRecords$): RedemptionRecords[] {
  return redemptionRecords$.filter(redemptionRecord => {
    const term = text.toLowerCase();
    return redemptionRecord.createdOn.toLowerCase().includes(term)
  });
}

@Component({
  selector: 'app-individual-user-page',
  templateUrl: './individual-user-page.component.html',
  styleUrls: ['./individual-user-page.component.scss']
})
export class IndividualUserPageComponent implements OnInit {

  public currentJustify = 'justified';
  public customerId = '';
  public outletId = '';
  public userId = '';
  public user;
  public moveIsClicked = false;
  public outlets = {};
  public newOutlet;
  filter = new FormControl('');
  public redemptionRecords$: Observable<RedemptionRecords[]>;
  constructor(private readonly usersService: UserService,
              private readonly route: ActivatedRoute,
              private readonly outletsService: OutletsService,
              private readonly notificator: NotificatorService,
              private readonly router: Router) { }

              @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

              onSort({column, direction}: SortEvent) {
            
                // resetting other headers
                this.headers.forEach(header => {
                  if (header.sortable !== column) {
                    header.direction = '';
                  }
                });
            
                this.redemptionRecords$.subscribe(
                  result => {
                    if (direction === '') {
                      this.redemptionRecords$ = of(result);
                    } else {
                      this.redemptionRecords$ = of([...result].sort((a, b) => {
                        const res = compare(a[column], b[column]);
                        return direction === 'asc' ? res : -res;
                      }));
                    }
                  }
                );
              }


  ngOnInit() {
    this.customerId = this.route.snapshot.params['id']
    this.outletId = this.route.snapshot.params['usersOfOutletId'];
    this.userId = this.route.snapshot.params['singleUserId'];
    this.route.data.subscribe(
      (result) => {
        this.user = result.user;
      }
    );
    this.usersService.getRedemptionRecordByUser(this.userId).subscribe(
      result => {
        console.log(result);
        this.redemptionRecords$ = this.filter.valueChanges.pipe(
          startWith(''),
          map(text => search(text,result))
        )
      }
    );
  }

  pickNewOutlet(location){
    console.log(`Customer: ${this.customerId}`);
    console.log(`Outlet: ${this.outletId}`);
    console.log(`User: ${this.userId}`);
    console.log(this.redemptionRecords$[0]);
    
    this.usersService.updateRelation(this.customerId, this.outletId, this.userId, location).subscribe(
      (result) => {
        this.router.navigate(['/adminpanel', this.customerId]);
        this.notificator.success(`User: ${this.user.name} has been moved to new location: ${location}`);
      }
    )
  }
  

  toggleMove() {
    this.moveIsClicked = !this.moveIsClicked;
    if (this.moveIsClicked === true) {
     this.route.data.subscribe(
       (result) => {
        this.outlets = result.outlets;
       }
     );
    }
  }

}
