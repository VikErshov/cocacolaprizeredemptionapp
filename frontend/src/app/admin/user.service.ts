import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { RedemptionRecords } from './interfaces/redemptionRecords';



@Injectable({
  providedIn: 'root'
})
export class UserService {


  constructor(
    private readonly http: HttpClient,
    ) {}

  public AllUsersOfAnOutlet(customerId: string, outletId: string): Observable<{users: User[]}> {
    return this.http.get<{users: User[]}>(`http://localhost:3000/customers/${customerId}/outlets/${outletId}/user`);
  }

  public individualUser(customerId: string, outletId: string, userId: string): Observable<{user: User}> {
    return this.http.get<{user: User}>(`http://localhost:3000/customers/${customerId}/outlets/${outletId}/user/${userId}`);
  }

  public createUser(customerId: string, outletId: string, name, email, password): Observable<{id, name, email, role}> {
    return this.http.post<{id, name, email, role}>(`http://localhost:3000/customers/${customerId}/outlets/${outletId}/user`, {
      name,
      email,
      password
    });
  }

  public getUserByUsername(username:string): Observable<{user: User}> {
    return this.http.get<{user: User}>(`http://localhost:3000/customers/${username}`);
  }
  
  public updateUser(customerId: string, outletId: string, userId: string, name: string, email: string, password: string): Observable<{id, name, email, role}> {
    return this.http.put<{id, name, email, role}>(`http://localhost:3000/customers/${customerId}/outlets/${outletId}/user/${userId}`, {
      name,
      email,
      password
    });
  }

  public updateRelation(customerId: string, outletId: string, userId: string, newRelation: string): Observable<{id, name, email, role}> {
    return this.http.put<{id, name, email, role}>(`http://localhost:3000/customers/${customerId}/outlets/${outletId}/${userId}/changerelation`, {
      newRelation,
    });
  }

  public deleteUser(customerId: string, outletId: string, userId: string): Observable<{id, name, email, role}> {
    return this.http.delete<{id, name, email, role}>(`http://localhost:3000/customers/${customerId}/outlets/${outletId}/user/${userId}`);
  }

  public getRedemptionRecordByUser(userId:string): Observable<RedemptionRecords[]>{
    return this.http.get<[]>(`http://localhost:3000/customers/redemption-records/${userId}`)
  }
}
