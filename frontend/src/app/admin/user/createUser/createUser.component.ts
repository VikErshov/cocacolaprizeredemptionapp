import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-createUser',
  templateUrl: './createUser.component.html',
  styleUrls: ['./createUser.component.css']
})
export class CreateUserComponent{

  constructor(private readonly activeModal: NgbActiveModal) { }

  passBack(name: string, email: string, password: string) {

    this.activeModal.close({name, email, password});
  }
}
