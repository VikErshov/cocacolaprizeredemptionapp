import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-updateUser',
  templateUrl: './updateUser.component.html',
  styleUrls: ['./updateUser.component.css']
})
export class UpdateUserComponent{

  constructor(private readonly activeModal: NgbActiveModal) { }

  @Input()
  public user;

  passBack(name: string, email: string, password: string) {

    this.activeModal.close({name, email, password});
  }
}
