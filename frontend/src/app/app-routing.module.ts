import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { ScannerComponent } from './scanner/scanner.component';
import { UserModule } from './user/user.module';


const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'adminpanel', loadChildren: './admin/admin.module#AdminModule'},
  {path: 'adminsecondarypanel', loadChildren: './admin-secondary/adminSecondary.module#AdminSecondaryModule'},
  {path: 'userpanel', loadChildren: './user/user.module#UserModule'},
  {path: 'userpanel/scan', component: ScannerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  {
    relativeLinkResolution: 'corrected'}),
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
