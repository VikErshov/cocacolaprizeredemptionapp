import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NavbarDropdownComponent } from './components/navbar/navbarDropdown/navbarDropdown.component';
import { FooterComponent } from './components/footer/footer.component';
import { HelpMessageModalComponent } from './components/navbar/helpMessageModal/helpMessageModal.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { ScannerModule } from './scanner/scanner.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { NotificatorService } from './core/services/notificator.service';
import { AuthService } from './core/services/auth.service';
import { StorageService } from './core/services/storage.service';


let notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
let auth = jasmine.createSpyObj('AuthService', ['login', 'logout', 'register']);

beforeEach(async(() => {
  TestBed.configureTestingModule({
    declarations: [
      AppComponent,
      HomeComponent,
      RegisterComponent,
      NavbarComponent,
      NavbarDropdownComponent,
      FooterComponent,
      HelpMessageModalComponent,
    ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      ToastrModule.forRoot(),
      BrowserAnimationsModule,
      HttpClientModule,
      CoreModule,
      SharedModule,
      ScannerModule,
      NgbModule.forRoot(),
      ZXingScannerModule,
      
    ],

    providers: [
      {
          provide: NotificatorService,
          useValue:notificator,
      },
      {
          provide: AuthService,
          useValue: auth,
      },
]
});


  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'frontend'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('frontend');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to frontend!');
  });
}));

