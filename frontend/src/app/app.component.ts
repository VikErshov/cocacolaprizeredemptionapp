import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificatorService } from './core/services/notificator.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './core/services/auth.service';
import { Subscription } from 'rxjs';
import { StorageService } from './core/services/storage.service';
import { CustomerService } from './admin/service';
import { SharedInfoService } from './core/services/sharedInfo.service';
import { HelpMessageModalComponent } from './components/navbar/helpMessageModal/helpMessageModal.component';
import { MessagesService } from './admin-secondary/message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(private readonly modalService: NgbModal,
              private readonly notificator: NotificatorService,
              private readonly router: Router,
              private readonly auth: AuthService,
              private readonly storage: StorageService,
              private route: ActivatedRoute,
              private readonly sharedInfoService: SharedInfoService,
              private readonly messageService: MessagesService,
  ) { }

  public isLoggedin = false;
  public username = '';
  public password = '';
  public role;
  public messageCount: number;
  private subscription: Subscription;

  openHelpMessageModal() {
    const modalRef = this.modalService.open(HelpMessageModalComponent, { size: 'lg', centered: true });
    const user = this.storage.get('username');
    modalRef.result.then((value) => {
      this.messageService.createMessage(value.issueTitle, value.description, user).subscribe(
          () => {
            this.notificator.success('You have successfully submitted a ticket!');
          }
        );
      });
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['/home']);
    this.notificator.success(`You have logged out.`);
  }

  ngOnInit() {

    this.subscription = this.auth.user$.subscribe(
      user => {

        if (user.username === null) {
          this.username = '';
          this.isLoggedin = false;
        } else {
          this.username = user.username;
          this.isLoggedin = true;
          this.role = user.role;
        }
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
