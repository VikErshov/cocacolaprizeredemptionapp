import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ScannerModule } from './scanner/scanner.module';
import { AdminModule } from './admin/admin.module';
import { TokenInterceptorService } from './auth/token-interceptor.service';
import { SharedModule } from './shared/shared.module';
import { CreateCustomerComponent } from './admin/customer/createCustomer/createCustomer.component';
import { UserModule } from './user/user.module';
import { NavbarDropdownComponent } from './components/navbar/navbarDropdown/navbarDropdown.component';
import { HelpMessageModalComponent } from './components/navbar/helpMessageModal/helpMessageModal.component';
import { FooterComponent } from './components/footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    NavbarComponent,
    NavbarDropdownComponent,
    FooterComponent,
    HelpMessageModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    ScannerModule,
    NgbModule.forRoot(),
    ZXingScannerModule,
    
  ],
  providers: [NgbActiveModal, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true,
  },
],
  entryComponents: [
    HelpMessageModalComponent,
 ],
  bootstrap: [AppComponent]
})
export class AppModule { }
