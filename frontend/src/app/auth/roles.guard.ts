import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from '../core/services/auth.service';
import { map, tap } from 'rxjs/operators';
import { NotificatorService } from '../core/services/notificator.service';

@Injectable({
  providedIn: 'root'
})
export class RolesGuard implements CanActivate {

  constructor(
    private readonly authService: AuthService,
    private readonly notificatorService: NotificatorService,
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    const expectedRole = next.data.expectedRole;

    return this.authService.user$.pipe(
      map(user => user.role === expectedRole),
      tap(user => {
        console.log(user);
        if (!user) {
          this.notificatorService.error(`You're unauthorized to access this page!`);
        }

      })
    );
  }
}
