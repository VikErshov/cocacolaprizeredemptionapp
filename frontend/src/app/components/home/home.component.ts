import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/core/services/auth.service';
import { NotificatorService } from 'app/core/services/notificator.service';
import { StorageService } from 'app/core/services/storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  private role: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly auth: AuthService,
    private readonly notificator: NotificatorService,
    private readonly storage: StorageService,
    private readonly router: Router,
  ) {}

  passBack(username: string, password: string) {
  this.auth.login(username, password).subscribe(

    (loginResult: any) => {
      this.notificator.success(`Welcome, ${loginResult.fullUser.name}!`);
      this.role = this.storage.get('role');
      if (this.storage.get('role') === 'admin') {
          this.router.navigate(['/adminpanel']);
      }
      if (this.storage.get('role') === 'user'){
        this.router.navigate(['/userpanel']);
      }

    },
    error => this.notificator.error(error.message),
  );
  }
}
