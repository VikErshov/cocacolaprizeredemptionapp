import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-helpMessageModal',
  templateUrl: './helpMessageModal.component.html',
  styleUrls: ['./helpMessageModal.component.scss']
})
export class HelpMessageModalComponent {

  constructor(private readonly activeModal: NgbActiveModal) { }

  passBack(issueTitle: string, description: string) {

    this.activeModal.close({issueTitle, description});
  }

}
