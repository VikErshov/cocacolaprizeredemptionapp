import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class NavbarComponent implements OnInit {

  @Input()
  public loggedIn;

  @Input()
  public role;

  @Input()
  public username;

  @Output()
  public helpMessage = new EventEmitter<undefined>();

  @Output()
  public logout = new EventEmitter<undefined>();

  constructor() { }

  ngOnInit() {
  }

  triggerLogout() {
    this.logout.emit();
  }

  triggerHelpMessage() {
    this.helpMessage.emit();
  }
}
