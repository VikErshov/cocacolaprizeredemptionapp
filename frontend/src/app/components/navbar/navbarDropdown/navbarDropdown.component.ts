import { Component, OnInit, Input } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-navbarDropdown',
  templateUrl: './navbarDropdown.component.html',
  styleUrls: ['./navbarDropdown.component.scss']
})
export class NavbarDropdownComponent {

  @Input()
  loggedIn;

  @Input()
  role;

  constructor(private readonly config: NgbDropdownConfig) {
    this.config.placement = 'bottom-left';
    this.config.autoClose = true;
  }

}
