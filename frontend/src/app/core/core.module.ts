import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NotificatorService } from './services/notificator.service';
import { StorageService } from './services/storage.service';
import { AuthService } from './services/auth.service';
import { AdminHomeComponent } from 'app/admin/admin-home/admin-home.component';
import { UserHomeComponent } from 'app/user/user-home.component';
import { SharedInfoService } from './services/sharedInfo.service';

@NgModule({

  providers: [
    NotificatorService,
    StorageService,
    HttpClient,
    AuthService,
    SharedInfoService,
  ]
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}