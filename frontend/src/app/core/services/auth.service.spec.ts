import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of, defer } from 'rxjs';
import { StorageService } from './storage.service';



describe('AuthService', () => {
     // dependencies mock

     const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);
     const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);

     // // this is used to errors from http
     // function asyncError<T>(errorObject: any) {
     //      return defer(() => Promise.reject(errorObject));
     // }

     // set dependencies to our mock implementation
     beforeEach(() => TestBed.configureTestingModule({
          providers: [
               {
                    provide: HttpClient,
                    useValue: http,
               },
               {
                    provide: StorageService,
                    useValue: storage,
               },
          ]
     }));

     it(`should be created`, () => {
          const service: AuthService = TestBed.get(AuthService);
          expect(service).toBeTruthy();
     });

     // it(`login should log the user in`, () => {

     //      // mock the return value of http.post
     //      http.post.and.returnValue(of({
     //           token: 'token',
     //           user: {
     //                name: 'test',
     //                role: 'user'
     //           },
     //      }));

     //      // call the login method and check if result matches the mock return (act/assert)
     //      const service: AuthService = TestBed.get(AuthService);
     //      service.login('name', 'password').subscribe(
     //           response => {
     //                expect(response.user.name).toBe('test');
     //                expect(response.token).toBe('token');
     //                expect(response.user.role).toBe('user');
     //           });
     // });

     // // test scenario 3
     // it(`login should call http.post`, () => {
     //      const service: AuthService = TestBed.get(AuthService);

     //      http.post.calls.reset();
     //      service.login('name', 'password').subscribe(
     //           () => expect(http.post).toHaveBeenCalledTimes(1)
     //      );
     // });

     // it(`login should call storage.set three times`, () => {
     //      const service: AuthService = TestBed.get(AuthService);

     //      storage.set.calls.reset();
     //      service.login('name', 'password').subscribe(
     //           () => expect(storage.set).toHaveBeenCalledTimes(3)
     //      );
     // });

     // it(`login should update the user subject`, () => {

     //      http.post.and.returnValue(of({
     //           token: 'token',
     //           user: {
     //                name: 'mancheew'
     //           }
     //      }));

     //      const service: AuthService = TestBed.get(AuthService);
     //      service.login('name', 'password').subscribe(
     //           () => {
     //                service.user$.subscribe(
     //                     (username) => expect(username).toBe('mancheew')
     //                );
     //           });
     // });

     // it(`logout should change the user subject to <null>`, () => {
     //      const service: AuthService = TestBed.get(AuthService);

     //      service.logout();

     //      service.user$.subscribe(
     //           (name) => expect(name).toBe(null)
     //      );
     // });

     // it(`logout should call storage.remove twice`, () => {
     //      const service: AuthService = TestBed.get(AuthService);
     //      storage.remove.calls.reset();

     //      service.logout();

     //      expect(storage.remove).toHaveBeenCalledTimes(3);
     //      expect(storage.remove).toHaveBeenCalledWith('token');
     //      expect(storage.remove).toHaveBeenCalledWith('username');
     //      expect(storage.remove).toHaveBeenCalledWith('role');

     // });

     // it(`register should call http.post`, () => {
     //      const service: AuthService = TestBed.get(AuthService);

     //      http.post.calls.reset();
     //      service.register('name', 'email', 'password').subscribe(
     //           () => expect(http.post).toHaveBeenCalledTimes(1)
     //      );
     // });

     // it(`register should create new user`, () => {
     //      const expectedUser = {
     //           name: 'Garfield',
     //           email: 'Garfield@abv.bg',
     //           password: 'C8hqyh1a@',

     //      };
     //      // mock the return value of http.post
     //      http.post.and.returnValue(of(expectedUser));

     //      // call the login method and check if result matches the mock return (act/assert)
     //      const service: AuthService = TestBed.get(AuthService);
     //      service.register('name', 'email', 'password').subscribe(
     //           response => {
     //                expect(response).toBe(expectedUser);
     //           });
     // });

     // it(`register should return an error when the server returns a 400`, () => {
     //      const errorResponse = new HttpErrorResponse({
     //           error: `test 400 error`,
     //           status: 400, statusText: `Bad Request`
     //      });

     //      http.post.and.returnValue(asyncError(errorResponse))

     //      const service: AuthService = TestBed.get(AuthService);
     //      service.register('name', 'email', 'password').subscribe(
     //           res => fail('expected an error'),
     //           err => expect(err.error).toBe('test 400 error'),
     //      )
     // });

});
