import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {tap} from 'rxjs/operators';
import { StorageService } from './storage.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly userSubject$ = new BehaviorSubject<any | null>({username: this.username, role: this.role, id: this.id});

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService
  ) { }

  public get user$() {
    return this.userSubject$.asObservable();
  }

  private get username(): string | null {
    const token = this.storage.get('token');
    const username = this.storage.get('username') || '';

    if (token) {
      return username;
    }

    return null;
  }

  private get role(): string | null {
    const token = this.storage.get('token');
    const role = this.storage.get('role') || '';

    if (token) {
      return role;
    }

    return null;
  }

  private get id(): string | null {
    const token = this.storage.get('id');
    const username = this.storage.get('id') || '';

    if (token) {
      return username;
    }

    return null;
  }

  public register(name: string, email: string, password: string) {
    return this.http.post('http://localhost:3000/register', {
      name,
      email,
      password
    });
  }

  public login(name: string, password: string) {
    return this.http
      .post('http://localhost:3000/login', {
        name,
        password
      })
      .pipe(
        tap((res: any) => {
          this.userSubject$.next({username: res.fullUser.name, role: res.fullUser.roles});
          this.storage.set('token', res.token);
          this.storage.set('username', res.fullUser.name);
          this.storage.set('role', res.fullUser.roles);
          this.storage.set('id', res.fullUser.id);
        })
      );
  }

  public logout(): void {
    this.storage.remove('token');
    this.storage.remove('username');
    this.storage.remove('role');
    this.storage.remove('id');
    this.userSubject$.next({username: null, role: null, id: null});
  }
}
