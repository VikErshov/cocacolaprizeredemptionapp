import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-error',
  templateUrl: './modal-error.component.html',
  styleUrls: ['./modal-error.component.scss'],
})
export class ModalErrorComponent implements OnInit {
  @Input()
  public message;
  @Input()
  public loseResult: string;
  ngOnInit() {
  }

  constructor(public activeModal: NgbActiveModal) {}

  
}



// import { Component, Input } from '@angular/core';
// import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

// @Component({
//   selector: 'app-modal-error',
//   template: `./modal-error.component.html`,
//   styleUrls: ['./modal-error.component.scss'],
// })
// export class ModalErrorComponent {
//   @Input() name;

//   constructor(public activeModal: NgbActiveModal) {}
// }

// @Component({
//   selector: 'ngbd-modal-component',
//   templateUrl: './modal-component.html'
// })
// export class NgbdModalComponent {
//   constructor(private modalService: NgbModal) {}

  
// }