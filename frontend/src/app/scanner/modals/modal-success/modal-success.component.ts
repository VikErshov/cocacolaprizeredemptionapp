import { Component, OnInit, Input, ViewChild, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PrizeEnum } from 'app/scanner/enum/prize-enum';
import { ScannerService } from 'app/scanner/scanner-service';
import { ScannerComponent } from 'app/scanner/scanner.component';

@Component({
  selector: 'app-modal-success',
  templateUrl: './modal-success.component.html',
  styleUrls: ['./modal-success.component.scss']
})
export class ModalSuccessComponent implements OnInit {
  @Input() prizes;
  @Input() isRedeem;
  @Input() barcode;
  // prizes:PrizeEnum ;
  
  constructor(private readonly activeModal: NgbActiveModal,
    private readonly scanner:ScannerService) { }

ngOnInit() {
  if(this.barcode === undefined){
    this.barcode = localStorage.getItem('barcode');
  }
  this.scanner.isRedeem(this.barcode).subscribe(
    (res)=>{
    this.isRedeem = res;
    console.log(this.isRedeem)
    }
  );
    console.log(this.isRedeem)
  this.scanner.getPrizes().subscribe(
    (result)=>{

      console.log(this.barcode);
      
      console.log(result);
      this.prizes = result;
    }
  );


}

  passBack(prize:string) {
    this.activeModal.close(prize);
    console.log();
  }

  close(){
    this.activeModal.close();
  }
}
