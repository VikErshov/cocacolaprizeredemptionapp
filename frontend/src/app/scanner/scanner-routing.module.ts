import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from '../components/home/home.component';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { UserHomeComponent } from 'app/user/user-home.component';
import { ScannerComponent } from './scanner.component';
import { RolesGuard } from 'app/auth/roles.guard';
import { AuthGuard } from 'app/auth/auth.guard';

const routes: Routes = [
{path: '', component: ScannerComponent, pathMatch: 'full', canActivate: [AuthGuard, RolesGuard], data: {
    expectedRole: 'user'
  },},
{path: 'scan', component: ScannerComponent},

];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class UsersRoutingModule { }
