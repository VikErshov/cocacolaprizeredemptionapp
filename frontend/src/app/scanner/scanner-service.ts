import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Barcode } from './interfaces/barcode';
import { User } from 'app/admin/interfaces/user';
import { Resolve } from '@angular/router';



@Injectable({
     providedIn: 'root'
})
export class ScannerService {


     constructor(
     private readonly http: HttpClient,
     ) {}


public scan(userId: string,barcodeContent:string): Observable<{barcode: string | undefined}> {
     return this.http.get<{barcode: string}>(`http://localhost:3000/${userId}/scan/${barcodeContent}`);
}

public redeem(userId:string, barcode:string, prize:string): Observable<{ barcode }> {
     
     return this.http.post<{barcode}>(`http://localhost:3000/${userId}/scan/${barcode}/redeem/${prize}`, { 
     barcode
     });
}

public getPrizes(): Observable<{}> {
     return this.http.get<{}>(`http://localhost:3000/prizes`);
}

public isRedeem(barcode:string): Observable<{barcode}>{
     return this.http.get<{barcode}>(`http://localhost:3000/${barcode}`)
}

//   public createCustomer(name: string): Observable<{id, name}> {
//     return this.http.post<{id, name}>('http://localhost:3000/customers', {
//       name
//     });
//   }

//   public updateCustomer(customerId: string, name: string): Observable<{name}> {
//     return this.http.put<{name}>(`http://localhost:3000/customers/${customerId}`, {
//       name
//     });
//   }

//   public deleteCustomer(customerId: string): Observable<{customer: Customer}> {
//     return this.http.delete<{customer: Customer}>(`http://localhost:3000/customers/${customerId}`);
//   }

}
