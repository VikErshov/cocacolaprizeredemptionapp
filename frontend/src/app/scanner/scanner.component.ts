import { Component, Input, Output, ViewChild, OnInit, AfterViewInit, EventEmitter  } from '@angular/core';
import { MatDialog } from '@angular/material';
import { BarcodeFormat } from '@zxing/library';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormatsDialogComponent } from './formats-dialog/formats-dialog.component';
import { AppInfoDialogComponent } from './scanner-info-dialog/scanner-info-dialog.component';
import { ScannerService } from './scanner-service';
import { UserService } from 'app/admin/user.service';
import { ActivatedRoute } from '@angular/router';
import { NotificatorService } from 'app/core/services/notificator.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalErrorComponent } from './modals/modal-error/modal-error.component';
import { ModalSuccessComponent } from './modals/modal-success/modal-success.component';

@Component({
     selector: 'scanner-root',
     templateUrl: 'scanner.component.html',
     styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements OnInit {
     public isCollapsed = false;
     public availableDevices: MediaDeviceInfo[];
     public currentDevice: MediaDeviceInfo = null;
     public userId = '';
     public founded = true;
     formatsEnabled: BarcodeFormat[] = [
          BarcodeFormat.CODE_128,
          BarcodeFormat.DATA_MATRIX,
          BarcodeFormat.EAN_13,
          BarcodeFormat.QR_CODE,
     ];
     public prizes;
     public isRedeem;
     
     public barcode;

     hasDevices: boolean;
     hasPermission: boolean;

     qrResultString: string;

     torchEnabled = false;
     torchAvailable$ = new BehaviorSubject<boolean>(false);
     tryHarder = false;

     constructor(private readonly _dialog: MatDialog,
          private readonly scanner:ScannerService,
          private readonly route: ActivatedRoute,
          private readonly notificator: NotificatorService,
          private modalService: NgbModal
          ) { }

     ngOnInit(){
     this.userId = localStorage.getItem('id');
     console.log(this.userId);
     
     
     }

     

     clearResult(): void {
          this.qrResultString = null;
     }

     onCamerasFound(devices: MediaDeviceInfo[]): void {
          this.availableDevices = devices;
          this.hasDevices = Boolean(devices && devices.length);

     }

     openWindowCustomClass(content) {
          this.modalService.open(content, { windowClass: 'dark-modal' });
     }
     

     onCodeResult(barcode: any): Observable<{any}>{
          this.qrResultString = barcode;
          console.log(barcode);
          this.scanner.isRedeem(barcode).subscribe(
               (bar)=>{
               this.isRedeem = bar;
               console.log(1);
               console.log(barcode);
               console.log(1);
               console.log(this.isRedeem)
                    }
               );
               this.scanner.getPrizes().subscribe(
               (redeem)=>{
          
               console.log(barcode);
               
               console.log(redeem);
               this.prizes = redeem;
               }
          );
          this.scanner.scan(this.userId,barcode).subscribe(
               (result) => {

                    const modalRef = this.modalService.open(ModalSuccessComponent);
                    modalRef.result.then((value) => {
     
                    this.scanner.redeem(this.userId,barcode,value).subscribe();
                    console.log(this.userId);
                    console.log(barcode)
                    console.log(value);
                    
                    }).catch((res) => console.log(res));
               
               },
          
               // (error) => {
               //      const modalRef = this.modalService.open(ModalErrorComponent);
               //      modalRef.componentInstance.message = 'You win a smile';
                    
               // });
               (error) => {
                    const modalRef = this.modalService.open(ModalErrorComponent);
                    modalRef.componentInstance.message = 'You win a smile :)';
               });
          localStorage.setItem('barcode',barcode);
          console.log(this.founded);
          return barcode;
     }

     

     onDeviceSelectChange(selected: string) {
          const device = this.availableDevices.find(x => x.deviceId === selected);
          this.currentDevice = device || null;
     }

     openFormatsDialog() {
          const data = {
               formatsEnabled: this.formatsEnabled,
          };

          this._dialog
               .open(FormatsDialogComponent, { data })
               .afterClosed()
               .subscribe(x => { if (x) { this.formatsEnabled = x; } });
     }

     onHasPermission(has: boolean) {
          this.hasPermission = has;
     }

     openInfoDialog() {
          const data = {
               hasDevices: this.hasDevices,
               hasPermission: this.hasPermission,
          };

          this._dialog.open(AppInfoDialogComponent, { data });
     }

     onTorchCompatible(isCompatible: boolean): void {
          this.torchAvailable$.next(isCompatible || false);
     }

     toggleTorch(): void {
          this.torchEnabled = !this.torchEnabled;
     }

     toggleTryHarder(): void {
          this.tryHarder = !this.tryHarder;
     }
}
