import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { MatListModule } from '@angular/material/list';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppInfoComponent } from './scanner-info/scanner-info.component';
import { AppComponent } from '../app.component';
import { FormatsDialogComponent } from './formats-dialog/formats-dialog.component';
import { AppInfoDialogComponent } from './scanner-info-dialog/scanner-info-dialog.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { ScannerComponent } from './scanner.component';
import { NgbModal, NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalSuccessComponent } from './modals/modal-success/modal-success.component';
import { ModalErrorComponent } from './modals/modal-error/modal-error.component';

@NgModule({
     imports: [

          // Angular
          BrowserModule,
          BrowserAnimationsModule,
          FormsModule,
          ReactiveFormsModule,
          NgbModule.forRoot(),
          // local
          NgbModule,
          ZXingScannerModule,
          NgbModalModule,
          // Material
          MatDialogModule,
          MatListModule,
          MatTooltipModule,
          MatButtonModule,
          MatFormFieldModule,
          MatSelectModule,
          MatInputModule,
          MatMenuModule,
          MatIconModule
     ],
     declarations: [ScannerComponent, FormatsDialogComponent, AppInfoComponent, AppInfoDialogComponent, ModalSuccessComponent, ModalErrorComponent ],
     bootstrap: [ScannerComponent],
     entryComponents: [FormatsDialogComponent, AppInfoDialogComponent,ScannerComponent,ModalSuccessComponent,ModalErrorComponent  ]
})
export class ScannerModule { }
