import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalErrorComponent } from 'app/scanner/modals/modal-error/modal-error.component';
import { ScannerService } from 'app/scanner/scanner-service';
import { ModalSuccessComponent } from 'app/scanner/modals/modal-success/modal-success.component';



@Component({
     selector: 'user-home',
     templateUrl: './user-home.component.html',
     styleUrls: ['./user-home.component.scss'],

})
export class UserHomeComponent implements OnInit {
     public userId = '';
     constructor(private modalService: NgbModal,
          private scanner: ScannerService) { }

     ngOnInit() {
          this.userId = localStorage.getItem('id');
     }



     openWindowCustomClass(content) {
          this.modalService.open(content, { windowClass: 'dark-modal' });
     }
     slides = [
          { img: "../../assets/coke-taste-the-feeling-14.jpg" },
          { img: "../../assets/coke-taste-the-feeling-14.jpg" }
     ];

     slideConfig = {
          "slidesToShow": 1, "slidesToScroll": 1, "speed": 3000, "infinite": true, "autoplay": true,
          "autoplaySpeed": 3000,
     };

     onCodeResult(resultString: string) {

          console.log(resultString);
          this.scanner.scan(this.userId, resultString).subscribe(
               (result) => {
                    const modalRef = this.modalService.open(ModalSuccessComponent);
                    modalRef.componentInstance.barcode = result;
                    modalRef.result.then((value) => {

                         this.scanner.redeem(this.userId, resultString, value).subscribe();
                         console.log(1);
                         console.log(result);
                         console.log(2);
                         console.log(this.userId);
                         console.log(resultString)
                         console.log(value);

                    }).catch((res) => console.log(res));

               },

               // (error) => {
               //      const modalRef = this.modalService.open(ModalErrorComponent);
               //      modalRef.componentInstance.message = 'You win a smile';

               // });
               (error) => {
                    const modalRef = this.modalService.open(ModalErrorComponent, { windowClass: 'dark-modal' });
                    modalRef.componentInstance.message = 'You win a smile :)';
               });

          // console.log(this.founded);

     }


}
