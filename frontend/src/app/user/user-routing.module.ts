import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { UserHomeComponent } from './user-home.component';
import { ScannerComponent } from 'app/scanner/scanner.component';
import { RolesGuard } from 'app/auth/roles.guard';


const routes: Routes = [
     {
          path: '', component: UserHomeComponent,
          canActivate: [AuthGuard, RolesGuard],
          data: {
               expectedRole: 'user'
          },
          pathMatch: 'full'

     },

     // {path: 'scan', component: ScannerComponent,
     //      canActivate: [AuthGuard,RolesGuard],
     //      data: {
     //           expectedRole: 'user'
     //      },
     // }
];

@NgModule({
     imports: [RouterModule.forChild(routes)],
     exports: [RouterModule]
})
export class UserRoutingModule { }
