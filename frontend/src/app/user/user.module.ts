import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbCollapseModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { UserHomeComponent } from './user-home.component';
import { UserRoutingModule } from './user-routing.module';
import { ScannerModule } from 'app/scanner/scanner.module';



@NgModule({
     declarations: [
          UserHomeComponent,
          
          
     ],
     imports: [
          SharedModule,
          FormsModule,
          UserRoutingModule,
          ReactiveFormsModule,
          NgbModule.forRoot(),
          NgbModule,
          NgbCollapseModule,
          NgbModalModule,
          
     ],
     entryComponents: [
          UserHomeComponent,
          
          
     ],
})
export class UserModule { }
